<%@page import="database.DatabaseManager"%>
<%@page import="oauthStrings.OauthStrings"%>
<%@page import="database.Client"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Elenco dei Client registrati</title>
</head>
<body>

<%

	DatabaseManager databaseManager;
	if(request.getSession().getAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER) != null){
		databaseManager = (DatabaseManager) request.getSession().getAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER);
	} else {
		databaseManager = new DatabaseManager();
	}

	%>
<table border="1">
	<tr><td>Name</td><td>redirectURI</td><td>clientID</td>
<%
	List<Client> clientList = databaseManager.getAllClient();
	for(int i=0; i<clientList.size(); i++) {
		String clientName = clientList.get(i).clientName;
		String redirectURI= clientList.get(i).redirectURI;
		long clientID = clientList.get(i).clientID;
%>
<tr>
<td><p><%=clientName %></p></td>
<td><p><%=redirectURI %></p></td>
<td><p><%=clientID %></p></td>
<td>
<form action="unregistration_client" method="post">
<input id="client_id" name="client_id" type="hidden" value="<%=clientID%>" /> 
<input id="url" name="url" type="hidden" value="list_client.jsp" /> 
<input id="submit" type="submit" value="Cancella" />
</form>
</td>
</tr>

<%
	}
%>
</table>
		
</body>
</html>