<%@page import="oauthStrings.OauthStrings"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registrazione Client</title>
<%
//variabili locali per accorciare le stringhe referenti ai name del form..
String clientName = OauthStrings.ClientRegistrationParameter.CLIENT_NAME;
String clientID = OauthStrings.ClientRegistrationParameter.CLIENT_ID;
String redirectURI = OauthStrings.ClientRegistrationParameter.REDIRECT_URI;
%>

<script type="text/javascript">
function trim(s) {
	return s.replace(/^\s+|\s+$/gm, "");
}

function verificaCampo() {
	
	var submit = document.getElementById("submit");
	var nodo = document.getElementById("redirect_uri");
	var testo = nodo.value;
	var errore = document.getElementById("errore_redirecturi");
	var ok = true;

	if(trim(testo) == "") {
		errore.innerHTML = "RedirectURI non pu� essere vuoto";
		submit.disabled = "disabled";
		ok = false;
	} else {
		errore.innerHTML = "";
	}
	
	nodo = document.getElementById("client_name");
	testo = nodo.value;
	errore = document.getElementById("errore_clientname");

	if(trim(testo) == "") {
		errore.innerHTML = "ClientName non pu� essere vuoto";
		submit.disabled = "disabled";
		ok = false;
	} else {
		errore.innerHTML = "";
	}
	
	if(ok)
		submit.removeAttribute("disabled");
}
</script>
</head>
<body>

<form action="registration_client" method="post">
<p>ClientName: <input id="<%=clientName%>" name="<%=clientName%>" type="text" value="" onchange="verificaCampo()" />  <span id="errore_clientname"></span></p>
<p>RedirectURI: <input id="<%=redirectURI%>" name="<%=redirectURI%>" type="text" value="" onchange="verificaCampo()" /> <span id="errore_redirecturi"></span></p>
<input id="submit" type="submit" value="Registra client" disabled="disabled" />
</form>

</body>
</html>