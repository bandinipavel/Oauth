<%@page import="oauthStrings.OauthStrings"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pagina di Login</title>
<script type="text/javascript">
function trim(s) {
	return s.replace(/^\s+|\s+$/gm, "");
}

function verificaCampo() {
	var submit = document.getElementById("submit");
	var nodo = document.getElementById("password");
	var testo = nodo.value;
	var errore = document.getElementById("errore_password");
	var ok = true;

	if(trim(testo) == "") {
		errore.innerHTML = "La password non pu� essere vuota";
		submit.disabled = "disabled";
		ok = false;
	} else {
		errore.innerHTML = "";
	}
	
	nodo = document.getElementById("username");
	testo = nodo.value;
	errore = document.getElementById("errore_username");

	if(trim(testo) == "") {
		errore.innerHTML = "Username non pu� essere vuoto";
		submit.disabled = "disabled";
		ok = false;
	} else {
		errore.innerHTML = "";
	}
	
	if(ok)
		submit.removeAttribute("disabled");
}
</script>
</head>
<body>
<%
	long clientID = 0;
 	if(request.getParameter("client_id") != null)		
		clientID = Long.parseLong(request.getParameter("client_id"));
	else if(request.getSession().getAttribute("client_id") != null)
		clientID = Long.parseLong((String)request.getSession().getAttribute("client_id"));
	else if(request.getAttribute("client_id") != null)
		clientID = Long.parseLong((String)request.getAttribute("client_id"));

	String errorMessage = (String)request.getAttribute(OauthStrings.Error.ERROR_MESSAGE);
	if(errorMessage == null)
		errorMessage = " ";
	
	String redirectURI = (String) request.getParameter("redirect_uri");
	
%>
<div>Resource Owner Login Page</div>
<form action="check_login" method="post">
<p>username: <input id="username" name="username" type="text" value="" onchange="verificaCampo()" />  <span id="errore_username"></span></p>
<p>password: <input id="password" name="password" type="password" value="" onchange="verificaCampo()" /> <span id="errore_password"></span></p>
<p>Autorizzi il client (<%=clientID %>) ad accedere ai propri dati? </p>
<p>
<input id="resource_owner_authorization" type="radio" name="resource_owner_authorization" value="allow">Autorizza
<input id="resource_owner_authorization" type="radio" name="resource_owner_authorization" value="deny">Nega
<p>
<p><input id="response_type" name="response_type" type="hidden" value="code" /> </p>
<p><input id="client_id" name="client_id" type="hidden" value="<%=clientID %>" /> </p>
<p><input id="redirect_uri" name="redirect_uri" type="hidden" value="<%=redirectURI %>" /> </p>
<input id="submit" type="submit" value="login" disabled="disabled" />
<div><%=errorMessage %></div>

</form>
</body>
</html>
