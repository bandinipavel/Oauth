<%@page import="oauthStrings.OauthStrings"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registrazione Effettuata correttamente</title>
</head>
<body>
<p>ClientName: <%=session.getAttribute(OauthStrings.ClientRegistrationParameter.CLIENT_NAME) %></p>
<p>RedirectURI: <%=session.getAttribute(OauthStrings.ClientRegistrationParameter.REDIRECT_URI) %></p>
<p>ClientID: <%=session.getAttribute(OauthStrings.ClientRegistrationParameter.CLIENT_ID) %></p>

</body>
</html>