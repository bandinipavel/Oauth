<%@page import="oauthStrings.OauthStrings"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin Page</title>
</head>
<body>

<p>Creazione nuovo database </p>
<p>Path di destinzione: <%=OauthStrings.DatabaseParameter.URL%> </p>
<form action="/Oauth/manager" method="get">
<input id="cmd1" name="cmd" type="hidden" value="create" /> 
<p>Nome<input id="arg1" name="arg" type="text" value="" /> <p>
<input id="submit1" type="submit" value="Crea Database" />
</form>

---------------------------------------------------------------------------------------------------------
<p>Cambio del database (non vengono fatti controlli sulla sua esistenza)</p>
<p>Attuale: <%=OauthStrings.ManagerParameter.DATABASE_NAME%> </p>
<form action="/Oauth/manager" method="get">
<p>Nuovo <input id="cmd2" name="cmd" type="hidden" value="select_database" /> 
<input id="arg2" name="arg" type="text" value="<%=OauthStrings.ManagerParameter.DATABASE_NAME%>" /> </p>
<input id="submit2" type="submit" value="Cambia Nome" />
</form>

---------------------------------------------------------------------------------------------------------
<p>Cambio del path del database (non vengono fatti controlli di correttezza)</p>
<p>Attuale: <%=OauthStrings.DatabaseParameter.URL%> </p>
<form action="/Oauth/manager" method="get">
<p>Nuovo <input id="cmd3" name="cmd" type="hidden" value="select_folder" /> 
<input id="arg3" name="arg" type="text" value="<%=OauthStrings.DatabaseParameter.URL%>" /> </p>
<input id="submit" type="submit" value="Cambia Path" />
</form>

</body>
</html>