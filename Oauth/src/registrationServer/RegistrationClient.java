package registrationServer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import log.Log;
import database.Client;
import database.DatabaseManager;
import oauthStrings.OauthStrings;
import errorManager.ErrorInfo;
import org.apache.commons.validator.routines.UrlValidator; 
/**
 * Servlet per la registrazione di nuovi Client.
 * 
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class RegistrationClient extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	 /**
     * Costruttore. 
     * 
     * Crea una servlet a partire da HttpServlet.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationClient() {
        super();
    }
    
    /**
     * Effettua un forward alla pagina di errore.
     * Per effettuare la registrazione bigona usare il metodo doPost.
     * 
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//imposta i campi di errore ed effettua il forward alla pagina di errore
		ErrorInfo error = new ErrorInfo( OauthStrings.Error.INVALID_REQUEST, "Metodo GET non gestito.");
		error.setAttributesToRequest(request);		
		request.getRequestDispatcher("/error.jsp").forward(request, response);
		return;
	}
	
    /**
	 * Metodo doPost della servlet.
	 * 
	 * Questo metodo gestisce le richieste di registrazione dei client, verificando la validit�
	 * del parametro 'redirecturi' e aggiungendolo quindi nel database.
	 * Se la registrazione viene effettuata con successo, viene effettuato il forward 
	 * ad una pagina comunicante che la registrazione � avvenuta con successo,
	 * altrimenti avviene il forward ad una pagina di errore.
	 * 
	 * 
	 * @param request   	La request della richiesta HTTP fatta alla servlet.
	 * @param response		La response tramite cui viene servita la richiesta.
	 * @return 		 		void. (I risultati vengono forniti tramite il forward della pagina).
	 * @throws				ServletException
	 * @throws 				IOException.
	 * @see 				HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * @see 				RegistrationClient#doPost(HttpServletRequest request, HttpServletResponse response)
	 * @see					HttpServletRequest
	 * @see 				HttpServletResponse
	 * @see 				DatabaseManager
	 */	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//recupera i parametri che devono essere forniti
		String clientName = (String) request.getParameter(OauthStrings.ClientRegistrationParameter.CLIENT_NAME);
		String redirectURI = (String) request.getParameter(OauthStrings.ClientRegistrationParameter.REDIRECT_URI);
		
		Log.log("clientID: " + clientName);
		Log.log("redirectURI: " + redirectURI);
		
		//verifica la validit� dell' URI fornito, 
		//se non lo �, effettua un forward ad pagina di errore con apposito messaggio
		if(!validURI(redirectURI)){
			ErrorInfo error = new ErrorInfo( OauthStrings.Error.REDIRECT_URI_MISMATCH, "L'URI non e' valido.");
			error.setAttributesToRequest(request);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;
			}

		//ottiene il databaseManager
		DatabaseManager databaseManager = DatabaseManager.getOrGenerateDatabaseManagerFromRequest(request);
		//verifica se il client non sia gi� registrato e,
		//se lo �, effettua un forward ad pagina di errore con apposito messaggio
		if(clientAlreadyPresent(clientName, databaseManager))	{
			ErrorInfo error = new ErrorInfo(OauthStrings.Error.CLIENT_ALREADY_REGISTERED,"Client gia' registrato.");
			error.setAttributesToRequest(request);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;
		}
		
		//registra il client nel database e
		//se non � possibile farlo, effettua un forward ad pagina di errore con apposito messaggio
		if(databaseManager.addClient(clientName,redirectURI) == null) {
			ErrorInfo error = new ErrorInfo(OauthStrings.Error.DATABASE_ERROR, "Errore nell'inserimento del client: " + clientName + ".");
			error.setAttributesToRequest(request);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;
			}
		
		//recupera il client appena registrato e		
		//se non � possibile recuperarlo, effettua un forward ad pagina di errore con apposito messaggio
		Client client = databaseManager.getClientByClientName(clientName);
		if(client == null) {
			ErrorInfo error = new ErrorInfo(OauthStrings.Error.DATABASE_ERROR, "Errore nell'estrazione del client: " + clientName + ".");
			error.setAttributesToRequest(request);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;
			}
		
		//salva i campi del client appena registrato nella sessione ed effettua il forward
		//alla pagina di "registrazione avvenuta con successo"
		request.getSession().setAttribute(OauthStrings.ClientRegistrationParameter.CLIENT_NAME, client.clientName);
		request.getSession().setAttribute(OauthStrings.ClientRegistrationParameter.REDIRECT_URI, client.redirectURI);
		request.getSession().setAttribute(OauthStrings.ClientRegistrationParameter.CLIENT_ID, client.clientID);
		request.getRequestDispatcher("registration_client_success.jsp").forward(request, response);
		return;

	}

	/**
	 * Verifica che la stringa passata sia un URI valido.
	 * 
	 * @param uri	da verificare.
	 * @return		true, se la stringa fornita � un URI valido<br>
	 * 				false altrimenti.
	 */
	private boolean validURI(String uri) {
		UrlValidator urlValidator = new UrlValidator();
		return urlValidator.isValid(uri);
	}
	
	/**
	 * Verifica se il client associato al clientID fornito � presente nel database, 
	 * usando databaseManager per effettuare le interrogazioni al database.
	 * 
	 * @param clientID		associato al client di cui si vuole verificare la presenza.
	 * @param databaseManager	utilizzato per l'interrogazione del database.
	 * @return					true, se il client associato al clientID fornito � presente nel database<br>
	 * 							false altrimenti.
	 */
	private boolean clientAlreadyPresent(String clientName, DatabaseManager databaseManager) {
		return (databaseManager.getClientByClientName(clientName) != null);
	}

	
}
