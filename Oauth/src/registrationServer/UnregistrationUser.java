package registrationServer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import log.Log;
import database.DatabaseManager;
import errorManager.ErrorInfo;
import oauthStrings.OauthStrings;

/**
 * Servlet per la cancellazione dei proprietari delle risorse.
 * 
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class UnregistrationUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	 /**
     * Costruttore. 
     * Crea una servlet a partire da HttpServlet.
     * @see HttpServlet#HttpServlet()
     */
    public UnregistrationUser() {
        super();
    }

    /**
     * Effettua un forward alla pagina di errore.
     * Per effettuare la disiscrizione si deve usare il metodo doPost.
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//GET non gestito
		ErrorInfo error = new ErrorInfo(OauthStrings.Error.INVALID_REQUEST, "Metodo GET non gestito.");
		error.setAttributesToRequest(request);
		
		request.getRequestDispatcher("/error.jsp").forward(request, response);
		return;

	}

	 /**
	  * 
	  * Metodo doPost della servlet.
	  * Questo metodo gestisce le richieste di cancellazione dei proprietari delle risorse.
	  * @param request   	La request della richiesta HTTP fatta alla servlet.
	  * @param response		La response tramite cui viene servita la richiesta.
	  * @return 		 	void. (I risultati vengono forniti tramite redirect della pagina).
	  * @throws				ServletException
	  * @throws 			IOException.
	  * @see 				HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	  * @see				HttpServletRequest
	  * @see 				HttpServletResponse
	  * @see 				DatabaseManager
	  */	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username =  request.getParameter(OauthStrings.ResourceOwnerRegistrationParameter.USERNAME);
		String url = request.getParameter("url");

		//cancella il proprietario delle risorse dal database, con tutte le relative autorizzazioni
		DatabaseManager.getOrGenerateDatabaseManagerFromRequest(request).deleteUser(username);
				
		//redireziona all'url passato
		Log.log("redirectUrl: " + url);
		response.sendRedirect(url);
	}

}
