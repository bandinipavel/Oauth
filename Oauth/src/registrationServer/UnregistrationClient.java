package registrationServer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import log.Log;
import oauthStrings.OauthStrings;
import database.DatabaseManager;

/**
 * Servlet per la cancellazione dei client.
 * 
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class UnregistrationClient extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	 /**
     * Costruttore. 
     * Crea una servlet a partire da HttpServlet.
     * @see HttpServlet#HttpServlet()
     */
    public UnregistrationClient() {
        super();
    }


    /**
     * Effettua un forward alla pagina di errore.
     * Per effettuare la registrazione si deve usare il metodo doPost.
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}


	 /**
	  * 
	  * Metodo doPost della servlet.
	  * Questo metodo gestisce le richieste di cancellazione dei proprietari delle risosre
	  * @param request   	La request della richiesta HTTP fatta alla servlet.
	  * @param response		La response tramite cui viene servita la richiesta.
	  * @return 		 		void. (I risultati vengono forniti tramite redirect della pagina).
	  * @throws				ServletException
	  * @throws 				IOException.
	  * @see 				HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	  * @see					HttpServletRequest
	  * @see 				HttpServletResponse
	  * @see 				DatabaseManager
	  */	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String clientID =  request.getParameter(OauthStrings.ClientRegistrationParameter.CLIENT_ID);
		String url = request.getParameter("url");

		//cancella il proprietario delle risorse dal database, con tutte le relative autorizzazioni
		Log.log("cancellazione client: " + clientID);
		DatabaseManager.getOrGenerateDatabaseManagerFromRequest(request).deleteClient(clientID);
				
		//redireziona all'url passato
		Log.log("reindirizzamento a: " + url);
		response.sendRedirect(url);
	}

}
