package registrationServer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseManager;
import oauthStrings.OauthStrings;
import errorManager.ErrorInfo;

/**
 * Servlet per la registrazione di nuovi proprietari delle risorse.
 * 
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class RegistrationUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	 /**
     * Costruttore. 
     * Crea una servlet a partire da HttpServlet.
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationUser() {
        super();
    }

    /**
     * Effettua un forward alla pagina di errore.
     * Per effettuare la registrazione si deve usare il metodo doPost.
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//GET non gestito
		ErrorInfo error = new ErrorInfo(OauthStrings.Error.INVALID_REQUEST, "Metodo GET non gestito.");
		error.setAttributesToRequest(request);
		
		request.getRequestDispatcher("/error.jsp").forward(request, response);
		return;
	}


    /**
	 * Metodo doPost della servlet.
	 * 
	 * Questo metodo gestisce le richieste di registrazione dei proprietari delle risorse,
	 *  verificando che non sia gia presente ed aggiungendolo quindi nel database.
	 * Se la registrazione viene effettuata con successo, viene effettuato il forward 
	 * ad una pagina predefinita per comunicare che la registrazione � avvenuta con successo,
	 * altrimenti avviene il forward ad una pagina di errore predefinita.
	 * 
	 * @param request   	La request della richiesta HTTP fatta alla servlet.
	 * @param response		La response tramite cui viene servita la richiesta.
	 * @return 		 		void. (I risultati vengono forniti tramite il forward della pagina).
	 * @throws				ServletException
	 * @throws 				IOException.
	 * @see 				HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * @see					HttpServletRequest
	 * @see 				HttpServletResponse
	 * @see 				DatabaseManager
	 */	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//recupera i parametri che devono essere forniti
		String username = (String) request.getParameter(OauthStrings.ResourceOwnerRegistrationParameter.USERNAME);
		String password = (String) request.getParameter(OauthStrings.ResourceOwnerRegistrationParameter.PASSWORD);
		
		//ottiene il databaseManager
		DatabaseManager databaseManager = DatabaseManager.getOrGenerateDatabaseManagerFromRequest(request);
	
		//verifica se il proprietario delle risorse non sia gi� registrato,
		//se lo �, effettua un forward ad pagina di errore con apposito messaggio
		if(databaseManager.getResourceOwnerByUsername(username) != null) {
			ErrorInfo error = new ErrorInfo(OauthStrings.Error.DATABASE_ERROR, "ResourceOwner gia' registrato.");
			error.setAttributesToRequest(request);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;
		}
		
		//registra il proprietario delle risorse nel database e
		//se non � possibile farlo, effettua un forward ad pagina di errore con apposito messaggio
		if(databaseManager.addResourceOwner(username,password) == null) {
			ErrorInfo error = new ErrorInfo(OauthStrings.Error.DATABASE_ERROR, "Errore nell'inserimento dell'user.");
			error.setAttributesToRequest(request);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;
		}
		
		//salva l'username del proprietario appena registrato nella sessione ed effettua il forward
		//alla pagina di "registrazione avvenuta con successo"
		request.getSession().setAttribute(OauthStrings.ResourceOwnerRegistrationParameter.USERNAME, username);
		request.getRequestDispatcher("registration_user_success.jsp").forward(request, response);
		return;

	}
}
