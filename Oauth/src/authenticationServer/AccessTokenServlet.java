package authenticationServer;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import log.Log;
import database.Authorization;
import database.Client;
import database.DatabaseManager;
import errorManager.ErrorInfo;
import oauthStrings.OauthStrings;

/**
 * Servlet per la fornitura di Access Token tramite richieste HTTP.
 * 
 * Questa servlet gestisce la fornitura di Access Token ai client sia tramite 
 * Authorization Token che Refresh Token. Le risposte sono effettuate tramite 
 * stringhe JSON e gli errori sono gestiti tramite forward ad una pagina di errore.
 *  
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class AccessTokenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
       
    /**
     * Costruttore. 
     * 
     * Crea una servlet a partire da HttpServlet.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public AccessTokenServlet() {
        super();
   }

	/**
	 * Metodo doGet della servlet.
	 * 
	 * Questo metodo gestisce le richieste di Access Token, verificandone prima la validit� 
	 * Questo viene individuato in un database, gestito tramite la classe DatabaseManager, 
	 * a partire dall'Authorition Token (o dal Refresh token) contenuto nella 
	 * richiesta, e viene fornito al richiedente sotto forma di stringa JSON contenente 
	 * tutti i valori necessari: <br>
	 * - access_token: stringa alfanumerica dell'Access Token;<br>
	 * - token_type: tipo specifico del token; <br>
	 * - expires_in: tempo di vita del token, oltre il quale perder� validit�;<br>
	 * - refresh_token: (opzionale) token per l'aggiornamento.<br><br>
	 *
	 * Se la richiesta non � valida, viene effettuato un forward alla pagina di errore error.jsp.
	 * Viene utilizzato un oggetto DatabaseManager ottenuto dalla sessione, se presente, 
	 * altrimenti ne viene generato uno nuovo.
	 * 
	 * 
	 * @param request   	La request della richiesta HTTP fatta alla servlet.
	 * @param response		La response tramite cui viene servita la richiesta.
	 * @return 		 		void. (I risultati vengono forniti tramite il parametro response).
	 * @throws				ServletException
	 * @throws 				IOException.
	 * @see 				HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * @see					HttpServletRequest
	 * @see 				HttpServletResponse
	 * @see 				DatabaseManager
	 */	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//crea una variabile per la raccolta di informazioni in caso di errori da comunicare in response
		ErrorInfo error = new ErrorInfo();

		//Estrae i parametri che devono essere forniti dalla request
		String grantType = (String) findAttribute(OauthStrings.AuthenticationParameter.GRANT_TYPE,request);
		
		String token = (String) findAttribute(OauthStrings.AuthenticationParameter.TOKEN,request);
		Log.log("token: "+token);
		String redirectURI = (String) findAttribute(OauthStrings.ClientRegistrationParameter.REDIRECT_URI,request);
		long clientID;
		try{
			clientID = Long.parseLong(findAttribute(OauthStrings.ClientRegistrationParameter.CLIENT_ID,request));
		}
		catch( NumberFormatException e){
			error.errorType=OauthStrings.Error.UNAUTHORIZED_CLIENT;
			error.errorDescription = "Client non autorizzato(parametro client non valido).";
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;
		}

		
		// cerca il DatabaseManager nella request e, se non lo trova, ne genera uno nuovo
		DatabaseManager databaseManager = DatabaseManager.getOrGenerateDatabaseManagerFromRequest(request);
		
		//dichiara la variabile che conterr� la stringa JSON da fornire
		String jsonResult = "";
		
		// Valida la richiesta. 
		// Se questa � valida 
		//		procede alla sua elaborazione, 
		// altrimenti
		//		comunica al client l'errore generato dalla richiesta tramite un forward alla 
		//		pagina di errore 'error.jsp'
		if(validateRequest(grantType, token, redirectURI, clientID, databaseManager, error)) {
			// La richiesta � valida
			
			
			//verifica se � stato fornito un Refresh Token
			if(grantType.equals(OauthStrings.ResourceManagerParameter.REFRESH_TOKEN)) {

				//genera l'authorization relativa al Refresh Token fornito e, se non � valido, 
				//effettua un forward alla pagina di errore, comunicando l'errore.
				Authorization authorization = databaseManager.refreshAuthorization(token);
				if(authorization == null){
					error = new ErrorInfo(OauthStrings.Error.UNAUTHORIZED_CLIENT, "refreshToken non valido.");
					Log.log("refreshToken non valido.");
					error.setAttributesToRequest(request);
					request.getRequestDispatcher("error.jsp").forward(request, response);
					return;	
				}
				
				
				//genera i campi della stringa JSON
				String accessToken = authorization.accessCode;
				String tokenType = getTokenType(token);
				long expiresIn = getAccessTokenExpireTimeByAuthCode(authorization.authCode,databaseManager);
				String refreshToken = authorization.refreshToken;
				
				//raggruppa i campi in una stringa JSON ben formata
				jsonResult = generateJsonResponse(accessToken, tokenType, expiresIn, refreshToken);

				
			} else 	//verifica se la richiesta � stata fatta fornendo un Authentication Token
				if(grantType.equals(OauthStrings.AuthenticationParameter.AUTHORIZATION_CODE)){

					//genera i campi della stringa JSON
					String accessToken = generateAccessToken(token,databaseManager);
					String tokenType = getTokenType(token);
					long expiresIn = getAccessTokenExpireTimeByAuthCode(token,databaseManager);
					String refreshToken = generateRefreshToken(token,databaseManager);
					
					//raggruppa i campi in una stringa JSON ben formata
					jsonResult = generateJsonResponse(accessToken, tokenType, expiresIn, refreshToken);
					
					//imposta l'Authentication Token come usato nel database
					databaseManager.setUsedAuthorization(token);
			}
			
		} else { // se la richiesta non � effettuata n� con un Authorization Token n� con un Refresh Token
				 // effettua il forward alla pagina di errore
			
			error.setAttributesToRequest(request);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;	
		}
		
		//imposta il content-type e la codifica della risposta
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		//invia la risposta e chiude la comunicazione
		response.getWriter().write(jsonResult);
		response.getWriter().close();
		return;
			
	}

	
	/**
	 * Reindirizza la richiesta al metodo doGet.
	 * @param request   	La request della richiesta HTTP fatta alla servlet.
	 * @param response		La response tramite cui viene servita la richiesta.
	 *  
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * @see AccessTokenServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Restituisce il valore associato dell'attributo o parametro passato all'interno 
	 * della request data. In caso non esistesse nessun attributo o parametro 
	 * associato alla stringa, restituisce null. 
	 * 
	 * @param attribute		Stringa contenete il nome dell'attibuto o parametro da cercare.
	 * @param request 		Request in cui cercare l'attributo o parametro passato.
	 * @return 				La stringa corrispondente all'attributo cercato.
	 * 
	 * @see 	HttpServletRequest
	 */
	private String findAttribute(String attribute, HttpServletRequest request) {
		
		//cerca nella session l'attributo cercato e se lo trova lo restituisce come risultato
		String result = (String) request.getSession().getAttribute(attribute);
		if(result != null)
			return result;

		//cerca nella session l'attributo cercato e se lo trova lo restituisce come risultato
		result = (String) request.getAttribute(attribute);
		if(result != null)
			return result;
		
		//se non � stato trovato nei precedenti lo cerca come parametro nella request, 
		//restituendo null in caso non lo trovi
		return (String) request.getParameter(attribute);
		
	}

	/**
	 * Verifica se i paramatri passati formano una valida autorizzazione a ricevere un'access token tramite Authentication Token.<br>
	 * 
	 * Come da specifica, viene controllato che:<br>
	 * - grantType sia uguale ad una stringa gi� predefinita;<br>
	 * - l'autorizzazione sia stata concessa al client e che non sia gia stato usato precedendemente;<br>
	 * - la redirectURI sia uguale a quella fornita al momento della registrazione.<br>
	 * Le verifiche vengono effettuate con interrogazioni al database tramite il DatabaseManager fornito.
	 * In caso di errore, vengono modificati appropriatamente i campi di Error e verr� restituito false. <br>
	 * In caso di successo, viene restituito true e non viene modificato error.
	 * 
	 * @param 	grantType		rappresentante il grant type della richiesta.
	 * @param 	authCode		rappresentante l'authentication token della richiesta.
	 * @param   redirectURI		rappresentante il redirect URI della richiesta.
	 * @param	clientID		rappresentante il clientID della della richiesta.
	 * @param	databaseManager utilizzato per le interrogazioni al database.
	 * @param	error			utilizzato per comunicare le informazioni necesarie in caso di errore
	 * @return  				true, se la richiesta � valida, false altrimenti. In quest'ultimo caso viene 
	 * 							modificato il valore di error, se non nullo.
	 * @see 	DatabaseManager
	 * @see 	ErrorInfo
	 */
	private boolean validateAccessTokenRequestByAuthorizationToken(String grantType, String authCode, String redirectURI, long clientID, DatabaseManager databaseManager, ErrorInfo error) {

		// per salvaguardare il codice, ma se error � null non verranno riportati errori
		// perch� questo nuovo valore di error rimarr� locale.
		if(error == null){
			error = new ErrorInfo();
		}
		
		// verifica che il grant type sia corretto, cio� uguale alla stringa predefinita OauthStrings.AuthenticationParameter.AUTHORIZATION_CODE
		// e, se questo non � verificato, imposta i campi di error e restituisce false.
		if(grantType==null || !grantType.equals(OauthStrings.AuthenticationParameter.AUTHORIZATION_CODE)){
			error.errorType=OauthStrings.Error.ACCESS_DENIED;
			error.errorDescription = "Grant Type errato.";
			return false;
		}
		
		// cerca nel database l'autorizzazione associata all'authentication token dato 
		// e, se questo non � presente, imposta i campi di error e restituisce false.
		Authorization authorization = databaseManager.getAuthorizationByAuthCode(authCode);
		if(authorization == null) {
			Log.log("auth null");
			error.errorType=OauthStrings.Error.UNAUTHORIZED_CLIENT;
			error.errorDescription = "Client non autorizzato (a).";
			return false;
		}

		//verifica che l'authentication token fornito non sia gia stato usato.
		//se lo �, imposta i campi di error e restituisce false.
		if(authorization.issued ){
			Log.log("auth already used");
			error.errorType=OauthStrings.Error.UNAUTHORIZED_CLIENT;
			error.errorDescription = "authcode gia' usato.";
			databaseManager.deleteAuthtorization(authCode);
			return false;
		}

		//verifica che l'authToken non sia scaduto
		if(authorization.authExpire.before(new Timestamp(System.currentTimeMillis())) ){
			Log.log("auth token scaduto: " + authCode);
			error.errorType=OauthStrings.Error.UNAUTHORIZED_CLIENT;
			error.errorDescription = "Client non autorizzato (authToken scaduto).";
			return false;
		}

		// ottiene le informazioni del client tramite una interrogazione del database.
		// Se queste non sono disponibili o se il client non � associato all'authentication code 
		//fornito autorizzato, imposta i campi di error e restituisce false.
		Client client = databaseManager.getClientByClientID(clientID);
		if(client == null || client.clientID != authorization.clientID) {
			Log.log("AccessTokenServlet:"+clientID +"!=" + authorization.clientID+"..");
			error.errorType=OauthStrings.Error.UNAUTHORIZED_CLIENT;
			error.errorDescription = "Client non autorizzato(b).";
			return false;
		}

		//verifica che il redirect URI passato sia uguale a quello registrato dal client
		//e, se non lo �, imposta i campi di error e restituisce false.
		if(redirectURI == null ||  !redirectURI.equals(authorization.redirectURI)){
			Log.log("AccessTokenServlet:"+redirectURI+"!=" + authorization.redirectURI+"..");
			error.errorType=OauthStrings.Error.REDIRECT_URI_MISMATCH;
			error.errorDescription = "Redirect URI errato.";
			return false;
		}
		
		//la richiesta � valida e viene restituito true, lasciando invariato error
		//a prima della richiesta
		return true;
	}

	/**
	 * Genera una stringa JSON rappresentante un access token
	 * 
	 * @param 	accessToken		 rappresentante la stringa identificativa dell'access token.
	 * @param 	tokenType		 rappresentante il tipo di permesso che l'access token rappresenta.
	 * @param   expiresIn		 rappresentante il tempo di vita dell'access token prima della sua revoca programmata.
	 * @param	refreshToken	 rappresentante il refresh token per l'aggiornamento del 
	 * @return  			     una stringa JSON formata dai valori passati.
	 */
	private String generateJsonResponse(String accessToken, String tokenType, long expiresIn, String refreshToken) {
		return "{" +
			   "\"access token_token\":\"" + accessToken +
			   "\", \"token_type\":\"" + tokenType +
			   "\", \"expires_in\":\"" + expiresIn +
			   "\", \"refresh_token\":\"" + refreshToken +  				 
			   "\"}";
	}
	
	
	/**
	 * Genera un refresh token ricercando nel database l'autorizzazione associata all'authorization token
	 * passato.
	 * 
	 * @param 	authCode 		rappresentante la stringa identificativa dell'authrorization token.
	 * @param 	databaseManager	utilizzato per l'interrogazione del database.
	 * @return  			    stringa contenente il refresh token associato all'autentication code,
	 * 							null se non esiste nessuna autorizzazione associata a tale authentication code.
	 * 
	 * @see 	DatabaseManager
	 */
	//TODO: da rendere pi� "templatica"
	private String generateRefreshToken(String authCode, DatabaseManager databaseManager) {
		Authorization authorization = databaseManager.getAuthorizationByAuthCode(authCode);
		if(authorization != null){
			return authorization.refreshToken;
		} else {
			return null;	
		}
	}
	
	/**
	 * Ritorna il tempo di vita dell'access token associato all'authCode fornito.
	 * 
	 * @param 	usato per identificare l'autorizzazione associata all'authCode nel database.
	 * @return	Una stringa rappresentante in decimale il tempo di vita del token.  			
	 */
	private long getAccessTokenExpireTimeByAuthCode(String authCode, DatabaseManager databasemanager) {
		Authorization authorization =  databasemanager.getAuthorizationByAuthCode(authCode);
		if(authorization != null && authorization.accessExpire != null) {
			return authorization.accessExpire.getTime()-System.currentTimeMillis();
		} else {
			return 0;
		}
	}
	
	
	/**
	 *  Ritorna il tipo dell'access token.
	 * 
	 * @param 	authCode usato per identificare l'autorizzazione, e quindi l'access token, associata.
	 * @return  Una stringa contenete il tipo dell'access token.
	 */
	//TODO: da rendere "templatica"
	private String getTokenType(String authCode) {
		return "file";
	}

	/**
	 * Genera un nuovo access token.
	 * La generazione viene fatta interrogando il database, dal quale si recupera la stringa 
	 * dell'access token dall'autorizzazione associata.
	 * 
	 * @param 	authCode 		rappresentante la stringa identificativa dell'authrorization token.
	 * @param 	databaseManager	utilizzato per l'interrogazione del database.
	 * @return  			    Una stringa contenente la stringa dell'access token.<br>
	 * 							null se non esiste un'autorizzazione associata.
	 */
	//TODO: da rendere "templatica"
	private String generateAccessToken(String authCode, DatabaseManager databaseManager) {
		Authorization authorization = databaseManager.getAuthorizationByAuthCode(authCode);
		return authorization.accessCode;
	}

	
	/**
	 * Verifica se i paramatri passati formano una valida autorizzazione a ricevere un'access token 
	 * tramite Authentication Token o tramite Refresh Token. 
	 * Nel secondo caso, non viene effettuato nessuna elaborazione di RedirectUri. 
	 * 
	 * @param 	grantType		rappresentante il grant type della richiesta.
	 * @param 	token			rappresentante il valore del token dello stesso tipo di grant type (auth.token o refresh token).
	 * @param 	redirectURI		rappresentante il redirect URI fornito dalla richiesta
	 * @param 	clientID		rappresentante il client ID fornito dalla richiesta
	 * @param 	databaseManager	utilizzato per le interrogazioni al database.
	 * @param 	error			utilizzato per comunicare le informazioni necesarie in caso di errore
	 * @return  			    
	 * 
	 * @see 	DatabaseManager
	 * @see 	ErrorInfo
	 */
	private boolean validateRequest(String grantType, String token, String redirectURI, long clientID, DatabaseManager databaseManager, ErrorInfo error) {
		
		//se non viene fornito un grant type valido, imposta i campi di errore e restituisce false.
		if(grantType == null || (!grantType.equals(OauthStrings.AuthenticationParameter.REFRESH_TOKEN)  
				&& !grantType.equals(OauthStrings.AuthenticationParameter.AUTHORIZATION_CODE)) ){
			error.errorType=OauthStrings.Error.ACCESS_DENIED;
			error.errorDescription = "Grant Type errato.(validateRequest)";
			return false;
		}
		
		//se � un grant type di tipo refresh token, delega all'apposita funzione la verifica,
		//altrimenti la delega alla funzione per la richiesta tramite auth.token.
		if(grantType.equals(OauthStrings.AuthenticationParameter.REFRESH_TOKEN)) {
			return validateAccessTokenRequestByRefreshToken(grantType, token, clientID, databaseManager, error);
		} else {
			return validateAccessTokenRequestByAuthorizationToken(grantType, token, redirectURI, clientID, databaseManager, error);
 			
		}
		
	}

	/**
	 * 
	 * Verifica se i paramatri passati formano una valida autorizzazione a ricevere un'access token tramite Refresh Token.<br>
	 * 
	 * In caso di successo, viene restituito true e non viene modificato error, altrimenti vengono impostati 
	 * i campi di error e viene restituito false.
	 * 
	 * @param 	grantType		rappresentante il grant type della richiesta.
	 * @param 	refreshToken	rappresentante il refresh token della richiesta.
	 * @param	clientID		rappresentante il clientID della della richiesta.
	 * @param	databaseManager utilizzato per le interrogazioni al database.
	 * @param	error			utilizzato per comunicare le informazioni necesarie in caso di errore.
	 * @return  				true, se la richiesta � valida, false altrimenti. In quest'ultimo caso viene 
	 * 							modificato il valore di error, se non � null.
	*/
	private boolean validateAccessTokenRequestByRefreshToken(String grantType, String refreshToken, long clientID, DatabaseManager databaseManager, ErrorInfo error) {

		//verifica che il grantType fornito sia valido
		if(grantType==null || !grantType.equals(OauthStrings.AuthenticationParameter.REFRESH_TOKEN)){
			error.errorType=OauthStrings.Error.ACCESS_DENIED;
			error.errorDescription = "Grant Type errato.";
			return false;
		}
		
		// verifica che esista una autorizzazione associata al refreshToken fornito
		Authorization authorization = databaseManager.getAuthorizationByRefreshToken(refreshToken);
		if(authorization == null) {
			Log.log("nessuna autenticazione con refresh dato: " + refreshToken);
			error.errorType=OauthStrings.Error.UNAUTHORIZED_CLIENT;
			error.errorDescription = "Client non autorizzato (refresh/a).";
			return false;
		}
		
		//verifica che il refreshToken non sia scaduto
		if(authorization.refreshExpire != null && authorization.refreshExpire.before(new Timestamp(System.currentTimeMillis())) ){
			Log.log("refresh token scaduto: " + refreshToken);
			error.errorType=OauthStrings.Error.UNAUTHORIZED_CLIENT;
			error.errorDescription = "Client non autorizzato (refreshToken scaduto).";
			return false;
		}

		// verifica che il client associato all'autenticazione sia lo stesso che ha appena fatto la richiesta
		Client client = databaseManager.getClientByClientID(clientID);
		if(client == null || client.clientID != authorization.clientID ) {
			Log.log("AccessTokenServlet:"+clientID +"!=" + authorization.clientID+"..");
			error.errorType=OauthStrings.Error.UNAUTHORIZED_CLIENT;
			error.errorDescription = "Client non autorizzato(refresh/b).";
			return false;
		}
		
		return true;		
	}

	
	
}
