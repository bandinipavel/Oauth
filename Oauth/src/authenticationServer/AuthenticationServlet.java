package authenticationServer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import log.Log;
import database.Client;
import database.DatabaseManager;
import oauthStrings.OauthStrings;
import errorManager.ErrorInfo;

/**
 * 	Servlet per la gestione e fornitura di Authentication Token.
 * 
 * Questa servlet gestisce la fornitura di Authentication Token ai client reindirizzando 
 * l'user agent (browser) ad una pagina specifica per permettere al proprietario delle risorse, 
 * dopo essersi autenticato, di concedere o meno l'autorizzazione al client di accedere alle proprie 
 * risorse.
 *  
 *   
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class AuthenticationServlet extends HttpServlet {

	public static final long serialVersionUID = 1L;
	  
    /**
     * Costruttore. 
     * 
     * Crea una servlet a partire da HttpServlet.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public AuthenticationServlet() {
        super();
    }

    /**
	 * Metodo doGet della servlet.
	 * 
	 * Questo metodo gestisce le richieste di Authentication Token, verificandone prima la validit�.
	 * 
	 * Se la richiesta � valida, l'user agent viene reindirizzato al redirectURI impostato dal client 
	 * al momento della registrazione, aggiungendo come parametri l'authorization code, il clientID, 
	 * il refresh code e l'username del proprietario delle risorse.<br>
	 * Se la richiesta non � valida, viene fatto un reindirizzamento ad una pagina di errore.
	 * 
	 * @param request   	La request della richiesta HTTP fatta alla servlet.
	 * @param response		La response tramite cui viene servita la richiesta.
	 * @return 		 		void. (I risultati vengono forniti tramite il parametro response).
	 * @throws				ServletException
	 * @throws 				IOException
	 * @see 				HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * @see					HttpServletRequest
	 * @see 				HttpServletResponse
	 * @see 				DatabaseManager
	 */	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ErrorInfo error = new ErrorInfo();
		
		//verifica che la richiesta sia ben formata
		if(!validateRequestParameters(request, error)) {
			
			//svuota la request dai parametri passati necessari alla verifica 
			//della richiesta (come da specifica)
			request.removeAttribute(OauthStrings.AuthenticationParameter.RESPONSE_TYPE);
			request.removeAttribute(OauthStrings.AuthenticationParameter.CLIENT_ID);
			
			// imposta i parametri di errore nella request e effettua il forward alla pagina di errore
			error.setAttributesToRequest(request);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;
			
			}
	
		//verifica che il client abbia l'autorizzazione o il deniego del proprietario delle risorse,
		//se non ce l'ha (ne autorizzata, ne negata)
		String resouseOwnerAuthorization = (String) request.getSession().getAttribute(OauthStrings.AuthenticationParameter.RESOURCE_OWNER_AUTHORIZATION); 
		if(resouseOwnerAuthorization == null) {
			
			//Se non ha una autorizzazione o un deniego 
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		
		//verifica che il proprietario abbia dato o negato la sua autorizzazione
		if(validateAuthorization(request)) {

			//rimuove la decisione dell'owner dalla sessione per evitare problemi di ripetizioni
			request.getSession().removeAttribute(OauthStrings.AuthenticationParameter.RESOURCE_OWNER_AUTHORIZATION); 
			
			//genera i campi di una nuova autorizzazione
			//String authCode = generateAuthCode(request);
			//String accessToken = generateAccessCode(authCode);
			//String refreshToken = generateRefreshToken(authCode);
			long clientID = Long.parseLong(request.getParameter(OauthStrings.AuthenticationParameter.CLIENT_ID));
			String username = (String) request.getParameter(OauthStrings.ResourceOwnerRegistrationParameter.USERNAME);
			
			//crea una nuova autorizzazione usando i campi sopra  e se non � possibile farlo, 
			//genera un errore ed effettua un forward alla pagina di errore
			String  authCode = DatabaseManager.getOrGenerateDatabaseManagerFromRequest(request).createAuthorization(clientID, username);
			if(authCode == null) {
				Log.log("error database");
				error = new ErrorInfo(OauthStrings.Error.DATABASE_ERROR, "Impossibile creare una nuova autenticazione.");
				error.setAttributesToRequest(request); 
				//
				request.getRequestDispatcher("error.jsp").forward(request, response);
				return;
				}
			
			//ricerca il redirectURI impostato al momento della registrazione
			String issuedClientURI = (String) request.getParameter(OauthStrings.AuthenticationParameter.REDIRECT_URI);

			//se e' stato stato registrato un redirectUri, effettua il redirect all'URL, aggiungendo 
			//i parametri code, clientID e refreshcode
			//altrimenti viene reidirizzato alla pagina iniziale e i campi vengono memorizzati in una sessione
			if(issuedClientURI != null){
				
				String state = (String) request.getAttribute(OauthStrings.AuthenticationParameter.STATE);
				//request.getRequestDispatcher(clientURI).forward(request, response);
				Log.log("redirect to: "+issuedClientURI);
				response.sendRedirect(issuedClientURI+"?code=" + authCode + "&state=" + state);
				return;

			} else {
				Log.log("empty issuedClientURI");
				//(non fa parte delle specifiche) utilizzato per mostrare informazioni di errori in jsp index.jsp
				request.getSession().setAttribute(OauthStrings.ClientRegistrationParameter.CLIENT_ID, clientID);
				request.getSession().setAttribute(OauthStrings.ResourceOwnerRegistrationParameter.USERNAME, username);
				request.getSession().setAttribute(OauthStrings.AuthenticationParameter.AUTH_TOKEN, authCode);
				
				request.getRequestDispatcher("index.jsp").forward(request, response);
				return;
			}
				
		} else {
			//rimuove la decisione dell'owner dalla sessione per evitare problemi di ripetizioni
			request.getSession().removeAttribute(OauthStrings.AuthenticationParameter.RESOURCE_OWNER_AUTHORIZATION); 

			//in caso non sia una richiesta valida, imposta i campi di errori ed effettua un forward alla pagina di errore
			error.errorType = OauthStrings.Error.ACCESS_DENIED;
			error.errorDescription = "Il Resource owner non ha autorizzato la richiesta.";		
			error.setAttributesToRequest(request); 
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;
		}
		
	}

	/**
	 * Reindirizza la richiesta al metodo doGet.
	 * 
	 * @param request   	La request della richiesta HTTP fatta alla servlet.
	 * @param response		La response tramite cui viene servita la richiesta.
	 *  
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * @see AccessTokenServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	/** 
	 *  Verifica che il proprietario delle risorse abbia concesso l'autorizzazione al client.
	 * 
	 * @param request   	La request della richiesta HTTP fatta alla servlet.
	 * @param response		La response tramite cui viene servita la richiesta.
	 *  
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * @see AccessTokenServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	private boolean validateAuthorization(HttpServletRequest request) {
	
		//verifica che l'attributo RESOURCE_OWNER_REQUEST  (memorizzato nella sessione) si impostato al valore "allow"
		String resourceOwnerRequest = (String) request.getSession().getAttribute(OauthStrings.AuthenticationParameter.RESOURCE_OWNER_AUTHORIZATION);
		if(resourceOwnerRequest != null && resourceOwnerRequest.equals(OauthStrings.AuthenticationParameter.ALLOW)) {
			return true;
		}
		return false;
	}
		
	
	/** 
	 *  Verifica che i campi forniti con la richiesta siano validi.
	 *  
	 * @param request   	La request della richiesta HTTP fatta alla servlet.
	 * @param response		La response tramite cui viene servita la richiesta.
	 * @return				true, se la i campi della richiesta sono validi, false altrimenti. 
	 *  
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * @see AccessTokenServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	private boolean validateRequestParameters(HttpServletRequest request, ErrorInfo error) {
		
		//verifica che il content-type sia corretto e, se non lo �, imposta i campi di errore e restituisce false.
		if(request.getContentType()== null  || !(new String("application/x-www-form-urlencoded")).equals(request.getContentType() )) {
			error.errorType = new String(OauthStrings.Error.INVALID_REQUEST);
			error.errorDescription =  new String("Richiesta non valida. Content-Type:" + request.getContentType() +".");
			return false;
		}
		
		//verifica che il parametro response_type si impostato a "code", e se non lo �, imposta i campi di errore e restituisce false.
		String responseType = (String)request.getParameter(OauthStrings.AuthenticationParameter.RESPONSE_TYPE);
		if( responseType == null || !responseType.toLowerCase().equals("code") ) {
			error.errorType = OauthStrings.Error.UNSUPPORTED_RESPONSE_TYPE;
			error.errorDescription =  "Response type non supportato: " + (String)request.getAttribute(OauthStrings.AuthenticationParameter.RESPONSE_TYPE) + ".";
			return false;
		}
	
		//ottiene il databaseManager
		DatabaseManager databaseManager = DatabaseManager.getOrGenerateDatabaseManagerFromRequest(request);
		
		//verifica che  il client esista e sia registrato, e se non lo �, imposta i campi di errore e restituisce false.
		long clientID = Long.parseLong(request.getParameter(OauthStrings.AuthenticationParameter.CLIENT_ID));
		
		if( !validClientID(clientID, databaseManager) ) {
			error.errorType = OauthStrings.Error.UNAUTHORIZED_CLIENT;
			error.errorDescription = "Client non esistente: " + (String)request.getParameter(OauthStrings.AuthenticationParameter.CLIENT_ID) + "." ;
			return false;
		}
		
		//verifica che il redirectURI fornito con la richiesta sia uguale a quello registrato dal client,
		//e se non lo �, imposta i campi di errore e restituisce false.
		String redirectURI = (String)request.getParameter(OauthStrings.AuthenticationParameter.REDIRECT_URI);
		if(!validRedirectURI(redirectURI, clientID, databaseManager)) {
			error.errorType =  OauthStrings.Error.INVALID_REQUEST;
			error.errorDescription =  "Richiesta non valida (URI non valido).";
			return false;
		}
		
		
		//tutti i campi sono validi, quindi restituisce true
		return true;
	}
	
	/** 
	 *  Verifica che il clientID fornito sia valido.
	 *  
	 * @param	clientID 		contenente il clientID del client che si vuole verificare.
	 * @param	databaseManager	usato per l'interrogazione del database.
	 * @return					true, se la il client � registrato, false altrimenti. 
	 *  
	 * @see DatabaseManager
	 */	
	private boolean validClientID(long clientID, DatabaseManager databaseManager) {
		//verifica che il client sia presente nel database e, se non lo �, restituisce false.
		if(databaseManager.getClientByClientID(clientID) == null ) {
			return false;
		}
		
		Log.log("Client validato: " + clientID);
		return true;
	
	}
	/** 
	 *  Verifica che il redirectURI e il clientID siano associati allo stesso client.
	 *  
	 * @param	issuedClientURI	di cui si vuole verificare che sia associato allo stesso client di clientID.
	 * @param	clientID		di cui si vuole verificare che sia associato allo stesso client di issuedClientURI.
	 * @param	databaseManager	da utilizzare per interrogare il database.
	 * @return					true, se redirectURI e clientID sono associati allo stesso client, false altrimenti. 
	 *  
	 * @see DatabaseManager
	 */	
	private boolean validRedirectURI(String issuedClientURI, long clientID, DatabaseManager databaseManager) {

		Log.log("issuedClientURI/ "+issuedClientURI);
		
		Client registeredClient = databaseManager.getClientByClientID(clientID);
		if(issuedClientURI == null && registeredClient.redirectURI == null) {
			return true;
		}
			
		if(issuedClientURI != null && issuedClientURI.equals(registeredClient.redirectURI)){
			return true;
		}

		return false;
	}
	
}
