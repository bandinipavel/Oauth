package loginServer;


import log.Log;
import database.DatabaseManager;
import database.ResourceOwner;

/**
 * Classe per la verifica delle credenziali di proprietario delle risorse.
 *   
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class Login {
 
	/**
	 * 	Verifica che la password fornita sia associata allo stesso proprietario delle risorse associato all'username.
	 * 
	 * 	@param username 	del proprietario delle risorse richiesto.
	 * 	@param password		di cui verificare la corrispondenza.
	 * 	@return 			true, se l'username e la password sono associati ad uno stesso proprietario delle risorse,<br>
	 * 						false altrimenti.
	 */
    public static boolean validateResourceOwnerCredential(String username, String password) {
    	
	    Log.log("Tentativo di login con: " + username+ " e " + password);
   	
	    //ottiene il proprietario delle risorse associato all'username fornito, 
	    //e se non esiste, se la password non � stata fornita o questa non � valida, 
	    //restituisce false
    	ResourceOwner user = new DatabaseManager().getResourceOwnerByUsername(username);
    	if(user == null || password == null || !DatabaseManager.HashPassword(password).equals(user.passwordHash)) {
    		return false;
    	}
    	
    	//le credenziali sono valide,
    	//quindi restituisce true.
    	return true;
    }




}
