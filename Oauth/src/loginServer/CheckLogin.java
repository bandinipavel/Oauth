package loginServer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseManager;
import log.Log;
import oauthStrings.OauthStrings;


/**
 * Servlet per l'autenticazione del proprietario delle risorse.
 * 
 * Questa servlet gestisce i parametri passati tramite request, autenticando il proprietario delle risorse
 * e gestendo la fase di concessione o diniego dell'autorizzazione al client che ne fa richiesta.
 *  
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class CheckLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 /**
     * Costruttore. 
     * 
     * Crea una servlet a partire da HttpServlet.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public CheckLogin() {
        super();
    }

	/**
	 * Reindirizza ad una pagina di errore.
	 * 
	 * L'autenticazione tramite  DEVE essere gestita solo dal metodo doPost.
	 * 
	 * @param request   	La request della richiesta HTTP fatta alla servlet.
	 * @param response		La response tramite cui viene servita la richiesta.
	 *  
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect(request.getContextPath() + "/error.jsp");
		return;
	}

	  /**
		 * Metodo doPost della servlet.
		 * 
		 * Questo metodo gestisce la validit� delle richieste di autenticazione dei proprietari delle risorse,
		 * verificandone la validit�.
		 * 
		 * Se la richiesta � valida, effettua un forward alla servlet di autenticazione per completare il rilascio 
		 * dell'autorizzazione.
		 * Se la richiesta non � valida, viene fatto un reindirizzamento alla pagina di login aggiungendo un messaggio 
		 * di errore.
		 * 
		 * @param request   	La request della richiesta HTTP fatta alla servlet.
		 * @param response		La response tramite cui viene servita la richiesta.
		 * @return 		 		void. (I risultati vengono forniti tramite il parametro response e il forwarding della richiesta).
		 * @throws				ServletException
		 * @throws 				IOException
		 * @see 				HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 * @see					HttpServletRequest
		 * @see 				HttpServletResponse
		 * @see 				DatabaseManager
		 */	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//imposta il content-type cifrato secondo la specifica di Oauth
		response.setContentType("application/x-www-form-urlencoded");  

		//recupera le credenziali del proprietario
		String username=request.getParameter(OauthStrings.ResourceOwnerRegistrationParameter.USERNAME);  
	    String password=request.getParameter(OauthStrings.ResourceOwnerRegistrationParameter.PASSWORD);  
	    
	    //verifica la vadidit� delle credenziali,
	    //Se sono valide, effettua un forward alla pagina di authenticazione,
	    //altrimenti, effettua un forward alla pagina di login per un'ulteriore tentativo.
	    if(Login.validateResourceOwnerCredential(username, password)) {
	    	Log.log("Autenticato: "+username+"/"+password);
	    	
	    	//recupera la decisione del proprietario delle risorse riguardante l'autorizzazione 
	    	//e la memorizza nella sessione di conseguenza
	    	String auth = (String) request.getParameter(OauthStrings.AuthenticationParameter.RESOURCE_OWNER_AUTHORIZATION); 
	    	if(auth != null && auth.equals(OauthStrings.AuthenticationParameter.ALLOW)) {
	    		Log.log("autenticato e autorizzato da: " + username + "/" + password);
	    		request.getSession().setAttribute(OauthStrings.AuthenticationParameter.RESOURCE_OWNER_AUTHORIZATION, OauthStrings.AuthenticationParameter.ALLOW);
	    	} else {
	    		request.getSession().setAttribute(OauthStrings.AuthenticationParameter.RESOURCE_OWNER_AUTHORIZATION,  OauthStrings.AuthenticationParameter.DENY);
	    	}

	    	
	    	Log.log("forward to authentication");
	    	//effettua il forward alla pagina di autenticazione 
	    	request.getRequestDispatcher("/authentication").forward(request, response);
			
	    } else {  
	    	Log.log("username o password errati.");
	   
	    	//rimuove i parametri forniti dal proprietario delle risorse
	    	request.removeAttribute(OauthStrings.ResourceOwnerRegistrationParameter.USERNAME);  
	    	request.removeAttribute(OauthStrings.ResourceOwnerRegistrationParameter.PASSWORD);  
	    	request.removeAttribute(OauthStrings.AuthenticationParameter.RESOURCE_OWNER_AUTHORIZATION); 

	    	//imposta l'errore da inviare alla pagina di login
	    	request.setAttribute(OauthStrings.Error.ERROR_MESSAGE, "Username o Password non validi.");
	    	
	    	// effettua il forward alla pagina di login
	    	request.getRequestDispatcher("login.jsp").forward(request,response);	    
	    }  
	          
	    response.getWriter().close();  
		
	}

}