package resourceServer;

import java.io.IOException;


import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import log.Log;
import database.Authorization;
import database.Client;
import database.DatabaseManager;
import errorManager.ErrorInfo;
import oauthStrings.OauthStrings;

/**
 * Servlet implementation class Resource
 */
public class Resource extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Resource() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//if (!autenticato())
		//	redirect login.jsp
		//
		//if any errors
		//	redirect.error
		//path = getPath (scope) {if null or invalid return error and null, if valid return userpath+scope}
		//if(path == null)
		//	redirect error.jsp
		//
		//sendFile()
		//*********************
		//variabile di ambiente per documenti: DBPath
		
		
		String accessToken = (String) request.getParameter(OauthStrings.ResourceManagerParameter.ACCESS_TOKEN);
		long clientID = Long.parseLong(request.getParameter(OauthStrings.AuthenticationParameter.CLIENT_ID));
		DatabaseManager databaseManager = getDatabaseManagerFromRequest(request);
		ErrorInfo error = null;
		
		if(!validAccessToken(accessToken,clientID,databaseManager)){
			error = new ErrorInfo(OauthStrings.Error.UNAUTHORIZED_CLIENT, "AccessToken non valido");
			setErrorAttributes(request, error);
			request.getRequestDispatcher("error.jsp").forward(request, response);
			return;
		}
		
		/*OLD Version
		String userFolder = databaseManager.getUserByAccessToken(accessToken);
		if(!sendFile(scope,userFolder, response)){
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		 */
		
		ResourceWorker resourceWorker = ResourceWorkerFactory.createResourceWorker();
		if(resourceWorker == null){
			response.sendError(503, "Impossibile risolvere la richiesta (Nessun Worker disponibile.)");
		} 
		
		if(!resourceWorker.send(request, response)){
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		
		return;
		
	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private boolean validAccessToken(String accessToken, long clientID,	DatabaseManager databaseManager) {
		
		Authorization authorization = databaseManager.getAuthorizationByAccessCode(accessToken);
		if(authorization == null){
			Log.log("resource.java: nessuna autorizzazione associata a " +accessToken);
			return false;
		}		
		
		Client client = databaseManager.getClientByClientID(authorization.clientID);
		Log.log("A.T. " + accessToken);
		
		//verifica che il client associato all'autorizzazione sia lo stesso associato alla richiesta
		if(client != null && client.clientID == clientID){
			// verifica che l'access token non sia scaduto.
			if(authorization.accessExpire == null || authorization.accessExpire.after(new Timestamp(System.currentTimeMillis())) ) {
				Log.log("access token non scaduto: " + authorization.accessCode);
				return true;
			}

			Log.log("access token scaduto: " + accessToken);
			//se l'access token � scaduto
			return false;
			
		}
		
		Log.log("client non valido: " + client.clientName);
		//se il client autorizzato non corrisponde a quello associato alla richiesta
		return false;

	}

	

	
	
	private DatabaseManager getDatabaseManagerFromRequest(HttpServletRequest request) {
		return DatabaseManager.getOrGenerateDatabaseManagerFromRequest(request);
		
	}

	private void setErrorAttributes(HttpServletRequest request, ErrorInfo error) {
		request.getSession().setAttribute(OauthStrings.Error.ERROR, error.errorType);
		request.getSession().setAttribute(OauthStrings.Error.ERROR_DESCRIPTION, error.errorDescription);
		request.getSession().setAttribute(OauthStrings.Error.ERROR_URI, error.errorURI);
	}

}
