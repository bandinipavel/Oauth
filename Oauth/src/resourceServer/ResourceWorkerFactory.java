package resourceServer;


/**
 * 	Classe Factory per la creazione dei worker.
 *  * 
 * 
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class ResourceWorkerFactory {

	/**
	 *  Crea un nuovo worker.
	 * 
	 *  @return riferimento al worker appena generato.
	 */
	public static ResourceWorker createResourceWorker() {
		return new ResourceWorkerImpl();
	}

}
