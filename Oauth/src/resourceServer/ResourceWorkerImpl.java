package resourceServer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import log.Log;
import oauthStrings.OauthStrings;
import database.DatabaseManager;
/**
 * 	Classe per la gestione delle risorse "file" contenute nella cartella "nome_proprietario" 
 *  ad un indirizzo "path" specifico.
 *  
 *  
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0 
 */
public class ResourceWorkerImpl implements ResourceWorker {
	
	/**
	 * 	Restituisce il path in cui vengono memorizzate le cartelle conteneti le risorse dei 
	 * 	proprietari delle risorse.
	 * 
	 * 	@return stringa contenete l'indirizzo a cui trovare le cartelle dei proprietari delle risorse
	 */
	private static String getResourseOwner() {
		if(System.getProperty("os.name").equals("win")){
    		return "C:\\Oauth\\Resources\\";
    	} else {
    		return "/home/malk/Oauth/Resources/";
    	}
	}

	/**
	 * Invia, tramite response, una risorsa identificabile dalle informazioni contenute in request. 
	 *
	 * @param request	contenente le info per l'identificazione della risorsa richiesta.
	 * @param response	tramite cui inviare la risorse richiesta.
	 * @return 	true, se la risorsa è stata inviata correttamente,<br>
	 * 			false, altrimenti.
	 * @see	HttpServletRequest
	 * @see	HttpServletResponse
	 */
	public boolean send( HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// recupera le informazioni necessarie per identificare la risorsa:
		// -il nome del proprietario delle risorse dall'access token,
		// -il nome del file dallo "scope".
		String accessToken = (String) request.getParameter(OauthStrings.ResourceManagerParameter.ACCESS_TOKEN);
		String fileName = (String) request.getParameter(OauthStrings.ResourceManagerParameter.SCOPE);

		// verifica che sia stata richiesta una risorsa,
		// resituendo false se non lo è stata.
		if(fileName == null){
			Log.log("filename nullo.");
			return false;
		}

		// ottiene il databaseManager per interrogare il database
		DatabaseManager databaseManager = getDatabaseManagerFromRequest(request);
		
		// ottiene il nome dell'utente, usato per identificare la cartella da cui ottenere le risorse
		// (si suppone che la request abbia passato la verifica di autorizzazione, e quindi che
		// il proprietario associato esista) 
		String userFolder = databaseManager.getResourceOwnerByAccessToken(accessToken).username;
		
		// genera l'indirizzo della risorsa richiesta e verifica che esista
		// restituendo false se non esiste, indicando il fallimento dell'invio.
		String filePath = getResourseOwner() + userFolder + "/" + fileName;
		File file = new File(filePath);
		if(!file.exists()) {
			Log.log("filename " + filePath + " non trovato");
			return false;
		}
		
		// genera gli stream di input (dalla risorsa) e di output (tramite li stream della response)
		FileInputStream fileInputStream = new FileInputStream(file);
		OutputStream responseOutputStream = response.getOutputStream();

		// imposta il Content-Type secondo le specifiche 
		response.setContentType("application/x-form-urlencoded");
		
		// imposta l'header della risposta come allegato
		response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
		
		// comunica la lunghezza della risorsa che si sta per inviare
		response.setContentLength((int) file.length());
		
		// Log.log("tipo di file inviato: " + URLConnection.guessContentTypeFromName(filePath));
		// response.setContentType(URLConnection.guessContentTypeFromName(filePath));
		
		// legge il file sotto forma di stream e lo invia tramite lo stream della response
		int bytes;
		while ((bytes = fileInputStream.read()) != -1) {
			responseOutputStream.write(bytes);
		}
		
		// al termine rilascia il file aperto e ritorna true indicando che l'invio e' stato
		// eseguito correttamente 
		fileInputStream.close();
		return true;
	}

	/**
	 * 	Ottiene dalla request fornita il databaseManager memorizzato. Se non ci fosse, ne crea 
	 * 	uno nuovo salvandolo nella request.
	 * 
	 * @param request	da cui ottenere o in cui salvare un DatabaseManager.
	 * @return			ritorna il databaseManager contenuto nella request o appena generato in essa.
	 * 					
	 */
	private DatabaseManager getDatabaseManagerFromRequest(HttpServletRequest request) {
		// recupera il databaseManager della request
		return DatabaseManager.getOrGenerateDatabaseManagerFromRequest(request);
	}
	
}
