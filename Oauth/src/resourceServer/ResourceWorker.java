package resourceServer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 	Interfaccia  dei worker impiegati nella fornitura delle risorse.
 * 
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public interface ResourceWorker {

	/**
	 * Invia, attraverso response, la risorsa richiesta tramite request.
	 *  
	 * @param request				contenente i parametri della richiesta effettuata dal client
	 * @param response				usato per inviare la risposta
	 * @return						true, se la risorsa � stata inviata correttamente,<br> false altrimenti
	 * @throws ServletException
	 * @throws IOException
	 */	
	boolean send(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

}
