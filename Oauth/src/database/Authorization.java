package database;

import java.sql.Timestamp;


/**
 * Classe per l'astrazione delle autorizzazioni concesse dai proprietari delle risorse ai client.
 * 
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class Authorization {

	public final String username;
	public final long clientID;
	public final String authCode;
	public final String refreshToken;
	public final String accessCode;
	public final String redirectURI;
	public final boolean issued;
	public final Timestamp authExpire;
	public final Timestamp accessExpire;
	public final Timestamp refreshExpire;
	
	/**
     * Costruttore unico. 
     * 
     * Crea un'autorizzazione aggregando i singoli campi.
	 * @param username 		del proprietario autorizzante
	 * @param clientID		del client autorizzato
	 * @param authCode 		associato all'autorizzazione
	 * @param refreshToken	attuale associato all'autorizzazione
	 * @param accessCode	attuale associato all'autorizzazione
	 * @param redirectURI	a cui deve essere reidirizzato il client quando richiede il primo access token
	 * @param issued		indicante se l'authcode � gi� stato fornito
	 * @param authExpire	indicante quando scadr� l'authorization token fornito.
	 * @param accessExpire	indicante quando scadr� l'accesstoken fornito.
	 * @param refreshExpire	indicante quando scadr� l'refreshtoken fornito.
	 */
	public Authorization(String username, long clientID, String authCode, String refreshToken, 
			String accessCode, String redirectURI, boolean issued, Timestamp authExpire, Timestamp accessExpire, Timestamp refreshExpire) {
		this.username = username;
		this.clientID = clientID;
		this.authCode = authCode;
		this.refreshToken = refreshToken;
		this.accessCode = accessCode;
		this.redirectURI = redirectURI;
		this.issued = issued;
		this.authExpire = authExpire;
		this.accessExpire = accessExpire;
		this.refreshExpire = refreshExpire;
		}
	
	
}
