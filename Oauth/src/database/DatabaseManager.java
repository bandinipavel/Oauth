package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
//import org.apache.derby.jdbc.EmbeddedDriver;

import log.Log;
import oauthStrings.OauthStrings;

/**
 * 	Classe per la gestione delle operazioni sul database.
 * 
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class DatabaseManager {

	//url contenente il riferimento per accedere al database desiderato
	private String url;
	
	//usato per la randomizzazione nella generazione di nuovi token
	private static final Random rand = new Random();
	
	/**
	 * Costruttore unico.
	 * 
	 * Memorizza le informazioni dell'URL e del Driver per la gestione della comunicazione
	 * con il database impostate nel file  "configuration.proprieties".
	 * 
	 */
	public DatabaseManager() {

		//ottiene le informazioni di url e Driver dal file "configuration.proprieties"
		//e cerca di caricare in memoria il driver corrispondente come nuova classe
		ResourceBundle bundle = ResourceBundle.getBundle("configuration");
		url = OauthStrings.DatabaseParameter.PROTOCOL + OauthStrings.DatabaseParameter.URL;
		String driver = bundle.getString(OauthStrings.DatabaseParameter.DRIVER);
		try {
			Log.log(driver);
			//Class.forName(driver).newInstance();
			DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
		} catch(Throwable t) { 
			Log.log("impossibile caricare EmbeddedDriver");
			t.printStackTrace();
			return;
		}
		
		/*
		String databaseName = OauthStrings.ManagerParameter.DATABASE_NAME;
		try {
			DriverManager.getConnection(url + databaseName + ";shutdown=true");
		} catch (SQLException e) {
			Log.log("impossibile chiudere database " + databaseName);
			e.printStackTrace();
			return;
		}
		*/
		return;
	
	}

	/**
	 * crea una connessione con il database.
	 * 
	 * @return 	Un oggetto Connection che permette l'interrogazione del database configurato.
	 * @see Connection
	 */	
	private Connection createConnection() {
	
		Connection c = null;
		String databaseName = OauthStrings.ManagerParameter.DATABASE_NAME;
				
		try {
			c = DriverManager.getConnection(url + databaseName + ";create=true");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		return c;
	
	}
	
	/**
	 * Chiude la connessione con il database e lo statement usato per le interrogazioni.
	 * 
	 * @param c	 	Connesction da chiudere.
	 * @param s		Statement da chiudere.
	 * 
	 * @see			Connection
	 * @see			Statement
	 */
	private void disconnect(Connection c, Statement s) {
		disconnect(c, s, null);
	}
	
	
	/**
	 * Chiude la connessione con il database, lo statement usato per le interrogazioni, 
	 * e il ResultSet usato per le risposte.
	 * 
	 * @param c	 	Connesction da chiudere.
	 * @param s		Statement da chiudere.
	 * @param r		ResultSet da chiudere.
	 * 
	 * @see			Connection
	 * @see			Statement
	 * @see 		ResultSet
	 */
	private void disconnect(Connection c, Statement s, ResultSet r) {
		try {
			r.close();
		} catch(Throwable t) {
			// Vuoto
		}
	
		try {
			s.close();
		} catch(Throwable t) {
			// Vuoto
		}
	
		try {
			c.close();
		} catch(Throwable t) {
			// Vuoto
		}
	}
	
	/**
	 * 	Genera un nuovo Authorization Code.
	 * 
	 *	@return una stringa contenente un nuovo Authorization Code.
	 */
	private String generateNewAuthCode() {
		return generateNewCode(Integer.toString(rand.nextInt()));
	}

	/**
	 * 	Genera un nuovo Access Code.
	 * 
	 *	@return una stringa contenente un nuovo authcode.
	 */
	private String generateNewAccessCode() {
		return generateNewCode(Integer.toString(rand.nextInt()));
	}
	
	/**
	 * 	Genera un nuovo Refresh Code.
	 * 
	 *	@return una stringa contenente un nuovo Refresh Code.
	 */
	private String generateNewRefreshCode() {
		return generateNewCode(Integer.toString(rand.nextInt()));
	}

	/**
	 * Genera una nuova authorizzazione a partire dai campi da impostare, restituendo il valore dell'authcode.
	 * In caso non fosse possibile creare l'autorizzazione verra' restituito null.
	 * 
	 * @param username		del prorietario delle risorse.
	 * @param clientID		del client.
	 * @param authCode		da impostare nella nuova autorizzazione.
	 * @param refreshToken	da impostare nella nuova autorizzazione.
	 * @param accessToken	da impostare nella nuova autorizzazione.
	 * @return				Lo stesso valore di AuthCode, se l'autorizzazione � stata creata nel database,<br>
	 * 						null altrimenti.
	 */
	private String createAuthorization(String username, long clientID, String authCode, String refreshToken, String accessToken) {

		Log.log("creating auth..");
		Log.log(username +" "+ clientID + " "+ authCode +" " +refreshToken);
		
		// restituisce null se  non ci sono i prerequisiti per trovare una autorizzazione, cioè che 
		// almeno username, AuthCode, AccessToken e RefreshToken non siano null.
		if( username == null ||  authCode == null || accessToken == null || refreshToken == null )
			return null;
		
		// Definisce connessione, statement da usare per la query al database 
		// e la variabile contenente l'authCode della nuova autorizzazione creata
		Connection c = null;
		PreparedStatement s = null;
		String authCodeResult = null;
		
		// controlla che esista un client associato al clientID fornito,
		// se non esiste restituisce null
		Client client = getClientByClientID(clientID);
		Log.log("DB, ricerca clientname..");
		if(client == null) {
			Log.log("db, client non trovato..");
			return null;
		}
		
		// ottiene il reredirectURI del client da autorizzare
		String redirectUri = client.redirectURI;
		
		// genera i timestamp dei Token associati alla nuova autorizzazione
		Timestamp authExpire    = new Timestamp(((long)System.currentTimeMillis()) + 1000l*60l*10l); // "tra 10 minuti"
		Timestamp accessExpire  = new Timestamp(((long)System.currentTimeMillis())+ 1000l*60l*60l*24l) ;// "tra un giorno"
		Timestamp refreshExpire = new Timestamp(((long)System.currentTimeMillis())+ 1000l*60l*60l*24l*30l);// "tra un mese"
				
		try {
				
			// esegue la query creando una nuova autorizzazione a partire dai valori forniti
			c = createConnection();
			s = c.prepareStatement("INSERT INTO AUTHORIZATIONS(username, clientid, authcode, refreshcode,accesstoken,redirecturi,isused,authexpire,accessexpire,refreshexpire) VALUES(?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			s.setString(1, username);
			s.setLong(2, clientID);
			s.setString(3, authCode);
			s.setString(4, refreshToken);
			s.setString(5, accessToken);
			s.setString(6, redirectUri);
			s.setInt(7, 0);
			s.setTimestamp(8, authExpire);
			s.setTimestamp(9, accessExpire);
			s.setTimestamp(10, refreshExpire );
			
			
			// controlla che siano state aggiunte righe al database (cioè aggiunta la nuova autorizzazione)
			// e, se cosi' e', imposta la variabile del risultato allo stesso valore dell'authcode 
			// dell'autorizzazione
			int r = s.executeUpdate();
			Log.log("creazione auth in db..");
			if(r>0) {
				Log.log("..inserito");
				authCodeResult = authCode;
			} 
			
			
		} catch(SQLException e) {
			
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s);		
		return authCodeResult;
	}

	/**
	 * 	Genera una nuova autorizzazione associata al client e proprietario forniti.
	 * 
	 * @param clientID	corrispondente al client nella nuova autorizzazione.
	 * @param username	corrispondente al proprietario delle risorse nella nuova autorizzazione.
	 * @return			Il valore corrispondente all'Authentication Code, se � stata creata 
	 * 					l'autorizzazione nel database<br>
	 * 					null, altrimenti.
	 */
	public String createAuthorization(long clientID, String username) {
		
		// genera i valori di authcode accessToken e refreshToken casuali e crea una nuova autorizzazione 
		// chiamando la funzione omonima a questa
		String authCode = generateNewAuthCode();
		String refreshToken = generateNewAccessCode();
		String accessToken = generateNewRefreshCode();
		return createAuthorization(username, clientID, authCode, refreshToken, accessToken);
		
	}
	
	/**
	 * 	Restituisce una lista di tutte le autorizzazioni concesse. 
	 * 
	 * 
	 * @return	List contenente tutte le autorizzazioni, 
	 * 			se e' stato possibile crearla.
	 */
	public List<Authorization> getAllAuthorization(){
		// crea la variabile per il risultato 
		List<Authorization> result = new ArrayList<Authorization>();
		
		// Definisce connessione, statement da usare per la query al database 
		// e la variabile contenente lil risultato
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		try {
			
			// esegue la query selezionando tutte le autorizzazioni.
			c = createConnection();
			s = c.prepareStatement("SELECT * FROM authorizations");
			r = s.executeQuery();
			
			// per ogni record estratto genera un oggetto Authorization contenete le informazioni 
			// dell'autorizzazione associata e la inserisce nella lista del risultato
			while(r.next()) {
	
				//String clientID = r.getString("CLIENTNAME");
				//String redirectURI = r.getString("REDIRECTURI");
				//String clientID = r.getString("CLIENTID");
				String username =   	r.getString("username");
				long clientName = 		r.getLong("clientID");
				String authCode =   	r.getString("authCode");
				String refreshToken =	r.getString("refreshcode");
				String accessCode = 	r.getString("accesstoken");
				String redirectURI =	r.getString("redirectURI");
				boolean isused =		r.getInt("isused") == 1;
				Timestamp authExpire = 	r.getTimestamp("authexpire");
				Timestamp accessExpire =r.getTimestamp("accessexpire");
				Timestamp refreshExpire=r.getTimestamp("refreshexpire");
				
				Authorization authorization = new Authorization(username,clientName, authCode, refreshToken, accessCode,redirectURI,isused, authExpire, accessExpire, refreshExpire);
				result.add(authorization);
	
			}
		} catch(SQLException e) {

			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s, r);
			e.printStackTrace();
			return null;
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		
		return result;
	}
	
	
	
	/**
	 * Ottiene l'astrazione dell'autenticazione corrispondente all'Authentication Code fornito.
	 * 
	 * @param accessCode	di cui si cerca l'autorizzazione.
	 * @return			L'autorizzazione riferita all'Authentication Code fornito, se esiste,<br>
	 * 					null, altrimenti.
	 * @see 			Authorization
	 */
	public Authorization getAuthorizationByAccessCode(String accessCode) {
		
		// verifica che l'accessCode fornito non sia nullo, 
		// restituendo null in caso lo sia
		Log.log("getAuthorizationByAccessCode/ "+ accessCode);
		if(accessCode == null)
			return null;
		
		// crea la variabile del risultato 
		Authorization result = null;
		
		// Definisce connessione, statement da usare per la query al database 
		// e la variabile contenente il risultato 
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		
		try {
			// esegue la query selezionando l'autorizzazione che e' associata 
			// all'access token fornito
			c = createConnection();
			s = c.prepareStatement("SELECT * FROM AUTHORIZATIONS WHERE ACCESSTOKEN = ?");
			s.setString(1, accessCode);
			r = s.executeQuery();
			
			// se l'autorizzazione esiste, crea un oggetto Authorization contenente 
			// tutte le info associate ad essa.
			if(r.next()) {
				String userName = r.getString("USERNAME");
				long clientID = r.getLong("CLIENTID");
				String authCode = r.getString("AUTHCODE");
				String refreshToken = r.getString("REFRESHCODE");
				//accessCode dato in input
				String redirectURI = r.getString("REDIRECTURI");
				boolean issued = r.getInt("ISUSED") == 1;
				Timestamp authExpire = 	r.getTimestamp("authexpire");
				Timestamp accessExpire =r.getTimestamp("accessexpire");
				Timestamp refreshExpire=r.getTimestamp("refreshexpire");
			
				Log.log("authorization recuperata	.");
				result = new Authorization(userName, clientID, authCode, refreshToken, accessCode, redirectURI, issued, authExpire, accessExpire, refreshExpire);
			} 
			
			
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s, r);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return result;
	}
	
	
	/**
	 * Ottiene l'astrazione dell'autenticazione corrispondente all'Authentication Code fornito.
	 * 
	 * @param authCode	di cui si cerca l'autorizzazione.
	 * @return			L'autorizzazione riferita all'Authentication Code fornito, se esiste,<br>
	 * 					null, altrimenti.
	 * @see 			Authorization
	 */
	public Authorization getAuthorizationByAuthCode(String authCode) {
		// verifica che l'authCode fornito non sia nullo, 
		// restituendo null in caso lo sia
		Log.log("getAuthorizationByAuthCode/ "+authCode);
		if(authCode == null)
			return null;
		
		// crea la variabile del risultato 
		Authorization result = null;
		
		// Definisce connessione, statement da usare per la query al database 
		// e la variabile contenente il risultato 
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		try {
			// esegue la query selezionando l'autorizzazione che e' associata 
			// all'authcode fornito
			c = createConnection();
			s = c.prepareStatement("SELECT * FROM AUTHORIZATIONS WHERE AUTHCODE = ?");
			s.setString(1, authCode);
			r = s.executeQuery();
			
			// se l'autorizzazione esiste, crea un oggetto Authorization contenente 
			// tutte le info associate ad essa.
			if(r.next()) {
				String userName = r.getString("USERNAME");
				long clientID = r.getLong("CLIENTID");
				//authcode dato in input
				String refreshToken = r.getString("REFRESHCODE");
				String accessCode = r.getString("ACCESSTOKEN");
				String redirectURI = r.getString("REDIRECTURI");
				boolean issued = r.getInt("ISUSED") == 1;
				Timestamp authExpire = 	r.getTimestamp("authexpire");
				Timestamp accessExpire =r.getTimestamp("accessexpire");
				Timestamp refreshExpire=r.getTimestamp("refreshexpire");
				Log.log("authorization recuperata	.");
				result = new Authorization(userName, clientID, authCode, refreshToken, accessCode, redirectURI, issued, authExpire, accessExpire, refreshExpire);
			} 
			
			
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s, r);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return result;
	}

	/**
	 * 	Ricerca l'autorizzazione corrispondente al refresh token fornito.
	 * 
	 * @param token 	refresh token di cui si vuole ottenere l'autorizzazione.
	 * @return			l'autorizzazione associata al refresh token, se esiste,<br>
	 * 					null altrimenti.
	 */
	public Authorization getAuthorizationByRefreshToken(String token) {
		// verifica che il refreshCode fornito non sia nullo, 
		// restituendo null in caso lo sia
		Log.log("getAuthorizationByRefreshToken/ "+token);
		if(token == null)
			return null;
		
		// crea la variabile del risultato 
		Authorization result = null;
				
		// Definisce connessione, statement da usare per la query al database 
		// e la variabile contenente il risultato 
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		try {
	
			// esegue la query selezionando l'autorizzazione che e' associata 
			// al refreshcode fornito
			c = createConnection();
			s = c.prepareStatement("SELECT * FROM AUTHORIZATIONS WHERE REFRESHCODE = ?");
			s.setString(1, token);
			r = s.executeQuery();
			
			// se l'autorizzazione esiste, crea un oggetto Authorization contenente 
			// tutte le info associate ad essa.
			if(r.next()) {
				String userName = r.getString("USERNAME");
				long clientID = r.getLong("CLIENTID");
				String authToken = r.getString("AUTHCODE");
				String refreshToken = r.getString("REFRESHCODE");
				String accessCode = r.getString("ACCESSTOKEN");
				String redirectURI = r.getString("REDIRECTURI");
				boolean issued = r.getInt("ISUSED") == 1;
				Timestamp authExpire = 	r.getTimestamp("authexpire");
				Timestamp accessExpire =r.getTimestamp("accessexpire");
				Timestamp refreshExpire=r.getTimestamp("refreshexpire");
				//    
				Log.log("authorization recuperata.");
				result = new Authorization(userName, clientID, authToken, refreshToken, accessCode, redirectURI, issued, authExpire, accessExpire, refreshExpire);
			} 
			
			
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s, r);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return result;
	}

	/**
	 * Imposta l'autorizzazione associata all'authentication code fornito come "gi� fornita"
	 * (in modo che non sia possibile riutilizzarla per ottenere lo stesso access token)
	 * 
	 * @param authCode usato per determinare l'autorizzazione da modificare.
	 * 
	 */	
	public void setUsedAuthorization(String authCode) {
		
		// verifica che l'authcode sia non nullo,
		// terminando la funzione se lo e'
		if(authCode== null)
			return;
		
		// Definisce connessione e statement da usare per la query al database 
		Connection c = null;
		PreparedStatement s = null;
		
		try {

			// esegue la query aggiornando la autorizzazione a "utilizzata"
			c = createConnection();
			s = c.prepareStatement("UPDATE AUTHORIZATIONS set isused=1 where authcode=?", Statement.RETURN_GENERATED_KEYS);
			s.setString(1, authCode);

			//verifica che sia stata aggiornata l'autorizzazione
			int r = s.executeUpdate();
			Log.log("set isUsed..");
			if(r>0) {
				Log.log("..set.");
			} 
			
			
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s);
	
		return;
		
	}


	/**
	 * Aggiorna l'autorizzazione associata all'access token fornito, creando nuovi 
	 * valori per il suo access token e il suo refresh token. 
	 * 
	 * @param 	refreshToken 	usato per determinare l'autorizzazione da modificare.
	 * @return 	Un'Authorization rappresentante l'autorizzazione dopo l'aggiornamento, 
	 * 			se e' stato possibile effettuare le modifiche,<br>
	 * 			null altrimenti.
	 */
	public Authorization refreshAuthorization(String refreshToken) {
		// verifica che il refreshCode fornito non sia nullo, 
		// restituendo null in caso lo sia
		if(refreshToken == null)
			return null;

		// crea la variabile del risultato 
		Authorization result = null;
						
		// definisce connessione, statement da usare per la query al database 
		Connection c = null;
		PreparedStatement s = null;
		
		// genera nuovi accessCode e RefreshCode con cui aggiornare l'autorizzazione
		// e i relativi nuovi timestamp
		String newAccessToken = generateNewAccessCode();
		String newRefreshCode = generateNewRefreshCode();
		Timestamp accessExpire  = new Timestamp(((long)System.currentTimeMillis())+ 1000l*60l*60l*24l) ;			 // "tra un giorno"
		Timestamp refreshExpire = new Timestamp(((long)System.currentTimeMillis())+ 1000l*60l*60l*24l*30l) ;		 // "tra un mese"
		Log.log("newAccessToken: " + newAccessToken);
		Log.log("newRefreshCOde: " + newRefreshCode);
		
		try {
	
			// esegue la query aggiornando l'autorizzazione relativa al refreshcode fornito
			// con nuovi accesscode e refreshcode
			c = createConnection();
			s = c.prepareStatement("UPDATE AUTHORIZATIONS SET REFRESHCODE = ?, ACCESSTOKEN = ?, ACCESSEXPIRE=?, REFRESHEXPIRE=? WHERE REFRESHCODE = ?");
			s.setString(1,newRefreshCode);
			s.setString(2,newAccessToken );
			s.setTimestamp(3, accessExpire);
			s.setTimestamp(4, refreshExpire);
			s.setString(5,refreshToken );
			s.executeUpdate();
			
		} catch(SQLException e) {
			
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, null);
		result = getAuthorizationByRefreshToken(newRefreshCode);
		return result;
	}
	

	/**
	 * 	Revoca l'autorizzazione associata all'Authentication Code fornito, invalidando anche gli Access Token 
	 *  e Refresh Token associati. 
	 * 
	 * @param 	authCode	associato all'autorizzazione da revocare.
	 * @return 	true, se � stato possibile revocare l'autorizzazione <br>
	 * 			false, altrimenti. 
	 */	
	public boolean deleteAuthtorization(String authCode) {

		// verifica che l'authcode fornito non sia nullo,
		// restituendo false se lo e' 
		if(authCode == null)
			return false;
		
		// definisce connessione, statement da usare per la query al database 
		// e la variabile contenente il risultato della query
		Connection c = null;
		PreparedStatement s = null;
		int r = 0;
		
		try {
	
			// esegue la query aggiornando l'autorizzazione relativa al refreshcode fornito
			// con nuovi accesscode e refreshcode
			c = createConnection();
			s = c.prepareStatement("DELETE FROM AUTHORIZATIONS WHERE AUTHCODE = ?");
			s.setString(1, authCode);
			r = s.executeUpdate();
			
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, null);

		// se l'autorizzazione esisteva ed e' stata rimossa restituisce true
		// altrimenti false
		if(r>0) {
			return true;
		}else{
			return false;
		}
	}


	/**
	 * Aggiunge un nuovo client al database.
	 * 
	 * @param 	clientID	da associare al nuovo client aggiunto.
	 * @param 	RedirectURI	da associare al nuovo client aggiunto
	 * @return				Restituisce un oggetto Client rappresentante il nuovo client, 
	 * 						se e' stato possibile aggiungerlo,
	 * 						null altrimenti
	 * @see 	Client
	 */	
	public Client addClient(String clientName, String RedirectURI)  {

		// definisce connessione, statement da usare per la query al database, 
		// la variabile contenente il risultato della query e il risultato della funzione
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		Client result = null;
		
		try {
			// esegue la query creando un nuovo client a partire dai parametri forniti
			c = createConnection();			
			s = c.prepareStatement("INSERT INTO CLIENTS(CLIENTNAME, REDIRECTURI) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
			s.setString(1, clientName);
			s.setString(2, RedirectURI);
			s.execute();
			
			// se è stato creato un nuovo client con successo crea un oggetto Client
			// contenete tutte le informazioni associate ad esso
			r = s.getGeneratedKeys();
			if(r.next()) {	
				long clientID = r.getLong(1);
				result = new Client(clientName, RedirectURI, clientID );
			}
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return result;
	}
	
	/**
	 * 	Restituisce una lista di tutti i client registrati. 
	 * 
	 * 
	 * @return	List contenente tutti i client registrati, 
	 * 			se � stato possibile crearla.
	 */
	public List<Client> getAllClient()  {

		// definisce connessione, statement da usare per la query al database, 
		// la variabile contenente il risultato della query e il risultato della funzione
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		List<Client> risultato = new ArrayList<>();
		
		try {
			// esegue la query selezionando tutti i client memorizzati
			c = createConnection();
			s = c.prepareStatement("SELECT * FROM CLIENTS");
			r = s.executeQuery();
			
			// per ogni client ottenuto dalla query si crea un oggetto Client associato
			// e lo si aggiunge alla lista del risultato
			while(r.next()) {
				String clientName = r.getString("CLIENTNAME");
				String redirectURI = r.getString("REDIRECTURI");
				long clientID = r.getLong("CLIENTID");
	
				Client client = new Client(clientName, redirectURI, clientID);
				risultato.add(client);
	
			}
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return risultato;
	}

	/**
	 *	Restituisce il client associato al clientID fornito.
	 * 
	 * @param clientID 	usato come riferimento per trovare il client.
	 * @return				Il client associato al clientID fornito, se esiste,<br>
	 * 						null altrimenti.
	 */
	public Client getClientByClientName(String clientName) {
		
		// verifica che il cientName non sia nullo,
		// restituendo null se lo e'
		if(clientName == null)
			return null;
		
		// definisce connessione, statement da usare per la query al database, 
		// la variabile contenente il risultato della query e il risultato della funzione
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		Client result = null;
		
		try {
			// esegue la query selezionando il client associato al parametro fornito
			c = createConnection();
			s = c.prepareStatement("SELECT * FROM CLIENTS WHERE CLIENTNAME = ?");
			s.setString(1, clientName);
			r = s.executeQuery();

			// se e' stato trovato un client crea un oggetto Client contenente
			// tutte le info associate ad esso 
			if(r.next()) {
				String redirectURI = r.getString("REDIRECTURI");
				long clientID= r.getLong("CLIENTID");
				result = new Client(clientName, redirectURI, clientID);
			} 
			
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return result;
	}
	
	/**
	 *	Restituisce il client associato al clientID fornito.
	 * 
	 * @param clientID	 	usato come riferimento per trovare il client.
	 * @return				Il client associato al clientID fornito, se esiste,<br>
	 * 						null altrimenti.
	 */
	public Client getClientByClientID(long clientID) {

		
		// definisce connessione, statement da usare per la query al database, 
		// la variabile contenente il risultato della query e il risultato della funzione		
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		Client result = null;


		try {
			// esegue la query selezionando il client associato al parametro fornito
			c = createConnection();
			s = c.prepareStatement("SELECT * FROM CLIENTS WHERE CLIENTID = ?");
			s.setLong(1, clientID);
			r = s.executeQuery();
			
			// se e' stato trovato un client crea un oggetto Client contenente
			// tutte le info associate ad esso 
			if(r.next()) {
				String redirectURI = r.getString("REDIRECTURI");
				String clientName= r.getString("CLIENTNAME");
				result = new Client(clientName, redirectURI, clientID);
			} 
			
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return result;
	}
	
	/**
	 * Aggiunge un nuovo proprietario delle risorse al database.
	 * 
	 * @param username	da associare al nuovo proprietario delle risorse aggiunto.
	 * @param password	da associare al nuovo proprietario delle risorse aggiunto.
	 * @return			Restituisce un oggetto ResourceOwner rappresentante il 
	 * 					nuovo proprietario delle risorse, se e' stato possibile aggiungerlo,<br>
	 * 					null altrimenti
	 * @see ResourceOwner
	 */
	public ResourceOwner addResourceOwner(String username, String password) {
		
		// verifica che i parametri passati non siano nulli,
		// restituendo null se lo sono
		if(username == null || password == null)
			return null;
		
		// definisce connessione, statement da usare per la query al database, 
		// la variabile contenente il risultato della query e il risultato della funzione		
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		ResourceOwner result = null;
		
		try {
			// esegue la query creando un nuovo proprietario a partire dai dati forniti
			c = createConnection();
			s = c.prepareStatement("INSERT INTO USERS(USERNAME, PASSWORD) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
			s.setString(1, username);
			password = HashPassword(password);
			s.setString(2, password);
			s.execute();
			
			// se e' stato creato un proprietario crea un oggetto ResourceOwner contenente
			// tutte le info associate ad esso 
			r = s.getGeneratedKeys();
			if(r.next())
				result = new ResourceOwner(username, password);
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return result;

		}
	
	/**
	 * 	Restituisce una lista di tutti i proprietari delle risorse registrati. 
	 * 
	 * 
	 * @return	List contenente tutti i proprietari delle risorse registrati, 
	 * 			se � stato possibile crearla.
	 */
	public List<ResourceOwner> getAllResourceOwner() {
		
		// definisce connessione, statement da usare per la query al database, 
		// la variabile contenente il risultato della query e il risultato della funzione	
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		List<ResourceOwner> risultato = new ArrayList<>();
		
		try {
			// esegue la query selezionando tutti i proprietari dal database
			c = createConnection();
			s = c.prepareStatement("SELECT * FROM USERS");
			r = s.executeQuery();
			
			//per ogni proprietario estratto, si crea un oggetto ResourceOwner contenente
			// tutte le info associate ad esso 
			while(r.next()) {	
				String username = r.getString("USERNAME");
				String password = r.getString("PASSWORD");
				risultato.add(new ResourceOwner(username, password));
	
			}
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
			return null;
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return risultato;
	}

	/**
	 *	Restituisce il proprietario delle risorse associato all'username fornito.
	 * 
	 * @param username	 	usato come riferimento per trovare il proprietario delle risorse.
	 * @return				Un oggetto ResourceOwner rappresentante il proprietario delle risorse 
	 * 						associato all'username fornito, se esiste,<br>
	 * 						null altrimenti.
	 * @see					ResourceOwner
	 */
	public ResourceOwner getResourceOwnerByUsername(String username) {
	
		// verifica che username non sia nullo,
		// restituendo null se lo e'
		if(username == null)
			return null;
		
		// definisce connessione, statement da usare per la query al database, 
		// la variabile contente il risultato della query e il risultato della funzione	
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		ResourceOwner result = null;
		
		try {
				
			// esegue la query selezionando il proprietario associato al parametro fornito
			c = createConnection();
			s = c.prepareStatement("SELECT * FROM USERS WHERE USERNAME = ?");
			s.setString(1, username);
			r = s.executeQuery();

			// se e' stato estratto un proprietario crea un oggetto ResourceOwner contente
			// tutte le info associate ad esso 
			if(r.next()) {
			    Log.log("user trovato: " + username);
				String password = r.getString("PASSWORD");
				Log.log("getUser passwordHash: "+ password);
				result = new ResourceOwner(username, password);
			} else {
				Log.log("user non trovato.");

			}
			
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return result;
	}

	/**
	 *	Restituisce il proprietario delle risorse tramite l'autorizzazione associata all'Access Token fornito.
	 * 
	 * @param accessToken	usato come riferimento per trovare il proprietario delle risorse.
	 * @return				Un oggetto ResourceOwner rappresentante il proprietario delle risorse 
	 * 						associato all'Access Token fornito, se esiste,<br>
	 * 						null altrimenti.
	 * @see					ResourceOwner
	 */
	public ResourceOwner getResourceOwnerByAccessToken(String accessToken) {

		// verifica che accessToken non sia nullo,
		// restituendo null se lo e'
		if(accessToken == null)
			return null;
		
		// definisce connessione, statement da usare per la query al database, 
		// la variabile contente il risultato della query e il risultato della funzione	
		Connection c = null;
		PreparedStatement s = null;
		ResultSet r = null;
		ResourceOwner result = null;

		try {
			// esegue la query selezionando l'autorizzazione associata al parametro fornito
			c = createConnection();
			s = c.prepareStatement("SELECT * FROM AUTHORIZATIONS WHERE ACCESSTOKEN = ?");
			s.setString(1, accessToken);
			r = s.executeQuery();
			
			// crea un oggetto ResourceOwner contente tutte le info associate all'utente 
			// dell'autorizzazione estratta, se questa esiste nel database
			if(r.next()) {
				String username = r.getString("USERNAME");
				result = getResourceOwnerByUsername(username);
			} 
			
		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database e restituisce il risultato dell'operazione
		disconnect(c, s, r);
		return result;
	}

	
	/**
	 * Genera un nuovo token casuale, utilizzabile come authentication/access/refresh token.
	 * 
	 * @param seed 	stringa per ulteriore randomizzazione del risultato.
	 * @return		Una stringa contenente un token.
	 */
	private static String generateNewCode(String seed){
		
		//genera una hashing in modo "complicato"		
		String[] generator = new String[2];
		generator[0] = seed;
		generator[1] = new Date().toString();
		
		String result = "Hashing";
		for(int i=0; i<generator.length; i++) {
			result  = Integer.toString(Integer.toString(new Random((result.hashCode() + generator[i]).hashCode() + rand.nextInt()).nextInt()).hashCode()); 
		}
		return result;
	}
	
	
	
	/**
	 * Effettua l'hashing della password passata.
	 * 
	 * @param password 	di cui effettuare l'hashing.
	 * @return			Una stringa che corrisponde all'hashing della password fornita.
	 */
	public static String HashPassword(String password) {
		// se password e' nullo, restituisce null
		// altrimenti genera il suo hashing usando 
		//la funzione hashCode della classe String
		if(password == null){
			Log.log("password vuota");
			return null;
		}
		return Integer.toString(password.hashCode());
	}

	
	/**
	 *  Crea, se non esiste, un oggetto DatabaseManager nella sessione; in entrambi i casi viene restituito 
	 *  come risultato.
	 * 
	 * @param request	in cui salvare, eventualmente, e recuperare il databaseManager.
	 * @return			Un oggetto databaseManager uguale a quello contenuto nella request, se era presente,
	 * 					uno nuovo altrimenti.
	 * 
	 * @see DatabaseManager 					
	 */
	public static DatabaseManager getOrGenerateDatabaseManagerFromRequest(HttpServletRequest request) {
		
		// se nella request non è definito l'attributo associato al databaseManager,
		// lo crea associandovi un nuovo oggetto DatabaseManager
		if(request.getSession().getAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER) == null)
			request.getSession().setAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER, new DatabaseManager());

		// restituisce il DatabaseManager associato all'attributo del databaseManager
		return (DatabaseManager) request.getSession().getAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER);
		
	}

	/**
	 *  Rimuove dal database il proprietario delle risorse associato all'username fornito,
	 *  insieme a tutte le autorizzazioni da lui concesse.
	 *  
	 * @param username associato al proprietario delle risorse da rimuovere.
	 */	
	public void deleteUser(String username) {
		// verifica che usernae non sia nullo,
		// terminando la funzione se lo e'
		if(username == null)
			return;
		
		// definisce connessione, statement da usare per la query al database, 
		Connection c = null;
		PreparedStatement s = null;

		try {
			// esegue la query eliminando dal database le autorizzazioni relative
			// al proprietario associato al parametro fornito
			c = createConnection();
			s = c.prepareStatement("DELETE FROM AUTHORIZATIONS WHERE USERNAME = ?");
			s.setString(1, username);
			s.executeUpdate();

			// esegue la query eliminando dal database il proprietario associato al parametro fornito
			s = c.prepareStatement("DELETE FROM USERS WHERE USERNAME = ?");
			s.setString(1, username);
			s.executeUpdate();
			

		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database
		disconnect(c, s);
		return;
	}

	/**
	 *  Rimuove dal database il client associato al clientID fornito,
	 *  insieme a tutte le autorizzazioni a lui concesse.
	 *  
	 * @param clientID associato al client che si vuole rimuovere.
	 */
	public void deleteClient(String clientID) {
		// verifica che clientID non sia nullo,
		// terminando la funzione se lo e'
		if(clientID == null)
			return;
		
		// definisce connessione, statement da usare per la query al database, 
		Connection c = null;
		PreparedStatement s = null;

		try {
			// esegue la query eliminando dal database le autorizzazioni relative
			// al client associato al parametro fornito	
			c = createConnection();
			s = c.prepareStatement("DELETE FROM AUTHORIZATIONS WHERE CLIENTID = ?");
			s.setString(1, clientID);
			s.executeUpdate();
			
			// esegue la query eliminando dal database il client associato al parametro fornito
			s = c.prepareStatement("DELETE FROM CLIENTS WHERE CLIENTID = ?");
			s.setString(1, clientID);
			s.executeUpdate();
			

		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database
		disconnect(c, s);
		return;
		
	}

	/**
	 *  Rimuove dal database il client associato al clientID fornito,
	 *  insieme a tutte le autorizzazioni a lui concesse.
	 *  
	 * @param clientID associato al client che si vuole rimuovere.
	 */
	public void deleteAuthorization(String authToken) {
		// verifica che authToken non sia nullo,
		// terminando la funzione se lo e'
		if(authToken == null)
			return;
		
		// definisce connessione, statement da usare per la query al database, 
		Connection c = null;
		PreparedStatement s = null;

		try {
			// esegue la query eliminando dal database le autorizzazioni relative al parametro fornito	
			c = createConnection();
			s = c.prepareStatement("DELETE FROM AUTHORIZATIONS WHERE AUTHCODE = ?");
			s.setString(1, authToken);
			s.executeUpdate();			

		} catch(SQLException e) {
			//in caso di errore si disconnette dal database e stampa a console lo stackTrace
			disconnect(c, s);
			e.printStackTrace();
		}
		
		// si disconnette dal database
		disconnect(c, s);
		return ;
		
	}

}
