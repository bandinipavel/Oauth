package database;

/**
 * Classe per l'astrazione dei client registrati.
 * 
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class Client {

	public final long clientID;
	public final String clientName;
	public final String redirectURI;


	/**
     * Costruttore unico. 
     * 
     * Crea un client aggregando i singoli campi.
	 * @param clientName	contenente il nome associato al client
	 * @param redirectURI	contenente l'URI a cui il client richiede essere reindirizzato
	 * @param clientID		associato univocamente al client
	 */
	public Client(String clientName, String redirectURI, long clientID) {
		this.clientName = clientName;
		this.redirectURI = redirectURI;
		this.clientID = clientID;

	}

}
