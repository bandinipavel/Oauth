package database;

/**
 * Classe per l'astrazione dei proprietari delle risorse registrati.
 * 
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class ResourceOwner {
	public final String username;
	public final String passwordHash;
	
	/**
     * Costruttore unico. 
     * 
     * Crea un proprietario delle risorse aggregando i singoli campi.
	 * 
	 * @param username		identificativo univoco del proprietario delle risorse
	 * @param passwordHash	contenente l'hashing della password 
	 * 
	 * NB:il parametro passwordHash � facilmente accessibile per "motivi accademici": 
	 * in realt� non dovrebbe mai lasciare il database. Per verificare la password
	 * bisognerebbe eseguire un comando SQL, ad esempio: 
	 * 		SELECT username FROM users WHERE USERNAME = username, PASSWORD = passwordHash;
	 * Alternativamente bisognerebbe creare uno spazio sicuro in cui effettuare le verifiche necessarie.
	 */
	public ResourceOwner(String username, String passwordHash){
		this.username = username;
		this.passwordHash = passwordHash;
	}
	
}
