	package errorManager;
import javax.servlet.http.HttpServletRequest;

import oauthStrings.OauthStrings;


/**
 * Classe per l'astrazione e la gestione delle informazioni di errori da comunicare.
 *   
 *  @author 			Paolo Bandini
 *  @version 			1.0
 *  @since				1.0
 */
public class ErrorInfo {
	public String errorType = "";
	public String errorDescription = "";
	public String errorURI = "";	
	
	/**
	 * Costruttore di default.
	 * 
	 * Crea un nuovo oggetto ErrorInfo impostando i tutti i parametri a "".
	 */
	public ErrorInfo() {
		
	}
	
	/** 
	 * Costruttore.
	 * 
	 * Crea un nuovo oggetto ErrorInfo impostando i tutti i parametri uguali a quelli forniti.
	 */
	public ErrorInfo(String errorType, String errorDescription, String  errorURI ) {
		this.errorType = errorType;
		this.errorDescription = errorDescription;
		this.errorURI = errorURI;
	}
	
	/**
	 * Costruttore.
	 * 
	 * Crea un nuovo oggetto ErrorInfo impostando i tutti i parametri,tranne errorURI, 
	 * uguali a quelli forniti.<br> 
	 * Il campo errorURI viene impostato a "".
	 */	
	public ErrorInfo(String errorType, String errorDescription) {
		this.errorType = errorType;
		this.errorDescription = errorDescription;
	}

	
	/**
	 * Converte l'oggetto in una String.
	 * 
	 * La stringa restituita rispecchia il pattern:<br>
	 * ErrorInfo: ERROR_TYPE, ERROR_DESCRIPTION, ERROR_URI <br>
	 * dove le stringhe in stampatello sono sostituite con i campi dell'oggetto.
	 */	
	public String toString() {
		return "ErrorInfo: " + errorType + ", " + errorDescription + ", " + errorURI;
	}

	/**
	 * Imposta i campi dell'oggetto come attributi della sessione inclusa nella request fornita.
	 * 
	 * @see 	request
	 */
	public void setAttributesToRequest(HttpServletRequest request) {
		request.getSession().setAttribute(OauthStrings.Error.ERROR, errorType);
		request.getSession().setAttribute(OauthStrings.Error.ERROR_DESCRIPTION, errorDescription);
		request.getSession().setAttribute(OauthStrings.Error.ERROR_URI, errorURI);

	}

}
