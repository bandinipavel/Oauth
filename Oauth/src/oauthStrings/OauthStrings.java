package oauthStrings;

import java.util.ResourceBundle;

/*
 *	Classe per l'aggregazione delle stringhe predefinite .
 * 
 * 	La funzione di questa classe e' quella memorizzare le stringhe predefinite del protocollo.
 * 	Le stringhe sono suddivise in sottoclassi rappresentanti il contesto in cui vengono usate.
 */
public class OauthStrings {
	
	public OauthStrings(){
		ResourceBundle bundle = ResourceBundle.getBundle("configuration");
		
		DatabaseParameter.PROTOCOL = bundle.getString("database.protocol");
		DatabaseParameter.URL = bundle.getString("database.url");
		ManagerParameter.DATABASE_NAME = bundle.getString("database.name");
	
	}
	
	public static final class Error {
		public static final String ERROR = "error";
		public static final String ERROR_DESCRIPTION = "error_description";
		public static final String ERROR_URI = "error_uri";
		public static final String UNSUPPORTED_RESPONSE_TYPE = "unsupported_response_type";
		public static final String UNAUTHORIZED_CLIENT = "unauthorized_client";
		public static final String INVALID_REQUEST = "invalid_request";
		public static final String ACCESS_DENIED = "access_denied";
		public static final String REDIRECT_URI_MISMATCH = "redirect_uri_mismatch";
		public static final String CLIENT_ALREADY_REGISTERED = "client_already_registered"; 
		public static final String DATABASE_ERROR = "database_error";
		public static final String ERROR_MESSAGE = "error_message";
	}

	public static final class AuthenticationParameter {
		public static final String REDIRECT_URI = "redirect_uri";
		public static final String RESPONSE_TYPE= "response_type";
		public static final String CLIENT_ID= "client_id";
		public static final String STATE = "state";
		public static final String RESOURCE_OWNER_AUTHORIZATION = "resource_owner_authorization";
		public static final String AUTH_TOKEN = "auth_token";
		public static final String ALLOW = "allow";
		public static final String DENY = "deny";
		public static final String GRANT_TYPE = "grant_type";
		public static final String AUTHORIZATION_CODE = "authorization_code";
		public static final String REFRESH_TOKEN = "refresh_token";
		public static final String ACCESS_TOKEN = "access_token";
		public static final String TOKEN = "token";
	}

	public static final class ClientRegistrationParameter {
		public static final String CLIENT_NAME = "client_name"; 
		public static final String CLIENT_ID= "client_id";
		public static final String REDIRECT_URI = "redirect_uri";
	}

	public static final class ResourceOwnerRegistrationParameter {
		public static final String USERNAME = "username"; 
		public static final String PASSWORD = "password";
	}

	public static final class DatabaseParameter {
		public static final String DATABASEMANAGER = "databasemanager";
		public static 		String PROTOCOL = "jdbc:derby:";
		public static 		String URL = "Oauth/Databases/";
		public static final String DRIVER = "database.embeddeddriver";
	}
	
	public static final class ResourceManagerParameter {
		public static final String SCOPE = "scope";
		public static final String ACCESS_TOKEN = "access_token";
		public static final String REFRESH_TOKEN = "refresh_token";
	}
	
	public static final class ManagerParameter {
		public static 		String DATABASE_NAME = "Oauth";
		public static final String COMMAND = "cmd";
		public static final String ARGUMENT = "arg";
		public static final String CREATE = "create";
		public static final String CONNECT = "connect";
		public static final String DISCONNECT = "disconnect";
		public static final String SELECT_DATABASE = "select_database";
		public static final String SELECT_FOLDER = "select_folder";
		
	}
	
}
