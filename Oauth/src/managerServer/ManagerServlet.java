package managerServer;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import errorManager.ErrorInfo;
import log.Log;
import oauthStrings.OauthStrings;

/**
 * 
 * @author malk
 *
 */
public class ManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//
	private String url = null;
	
	/**
	 * 
	 */
    public ManagerServlet() {
        super();
        
		ResourceBundle bundle = ResourceBundle.getBundle("configuration");
		String driver = bundle.getString("database.embeddeddriver");		
		url = OauthStrings.DatabaseParameter.PROTOCOL 
				+ OauthStrings.DatabaseParameter.URL;

		try {
			Log.log("caricamento driver");
			Class.forName(driver).newInstance();
			Log.log(driver);
		} catch(Throwable t) { 
			Log.log("driver " + driver + "non caricato");
			t.printStackTrace();
		}
    }

    /**
     * 
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	
	/**
	 * 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cmd = request.getParameter(OauthStrings.ManagerParameter.COMMAND);
		String arg = request.getParameter(OauthStrings.ManagerParameter.ARGUMENT);

		if(cmd == null){
			Log.log("cmd nullo"); 
			return;
		}
		
		boolean errorOccured = false;
		
		if(cmd.equalsIgnoreCase(OauthStrings.ManagerParameter.CREATE)){
			
			boolean created = createNewDatabase(arg);
			if(!created){
				ErrorInfo error = new ErrorInfo("database_error", "impossibile creare database: " + arg);
				error.setAttributesToRequest(request);
				errorOccured = true;
			}
			
		} else if(cmd.equalsIgnoreCase(OauthStrings.ManagerParameter.SELECT_DATABASE)){
			selectDatabase(arg);

		} else if(cmd.equalsIgnoreCase(OauthStrings.ManagerParameter.SELECT_FOLDER)){
			selectFolder(arg);
			
		} else{
				errorOccured = true;
		}

		if(errorOccured){
			request.getRequestDispatcher("error.jsp").forward(request, response);
		}else{
			request.getRequestDispatcher("admin.jsp").forward(request, response);		
		}
	}
	
	
	protected void selectFolder(String folder) {
		if(folder == null) {
			Log.log("folder null");
			return;
		}

		OauthStrings.DatabaseParameter.URL = folder;
		return;
	}

	/**
	 * 
	 * @param databaseName
	 */
	protected void selectDatabase(String databaseName) {
		// se non viene fornito alcun nome, non viene modificato nulla
		if(databaseName == null) {
			Log.log("databaseName nullo");
			return;
		}	
		
		OauthStrings.ManagerParameter.DATABASE_NAME = databaseName;
		Log.log("Cambiato database in "+ databaseName);
	}
	
	/**
	 * 
	 * @param databaseName
	 */
	protected boolean createNewDatabase(String databaseName) {
		if(databaseName == null){
			Log.log("databaseName nullo");
			return false;
		}

		boolean created = false;
		try {
			Connection c = null;
			PreparedStatement s = null;
			c = DriverManager.getConnection(url + databaseName + ";create=true");

			s= c.prepareStatement("create table users (username varchar(256), password int)");
			s.execute();
			s= c.prepareStatement("create table clients (clientName varchar(256), redirectURI varchar (256), clientID int generated always as identity)");
			s.execute();
			s= c.prepareStatement("create table authorizations ( username varchar(256), authcode varchar(256), accesstoken varchar(256), refreshcode varchar(256),"
			+ "redirectURI varchar(256), isused int, authexpire timestamp, accessexpire timestamp, refreshexpire timestamp, clientID int)");
			s.execute();

			s= c.prepareStatement("alter table users add unique (username)");
			s.execute();
			s= c.prepareStatement("alter table users alter column username not null");
			s.execute();
			s= c.prepareStatement("alter table users alter column password not null");
			s.execute();

			s= c.prepareStatement("alter table clients add unique (clientID)");
			s.execute();
			s= c.prepareStatement("alter table clients add unique (clientName)");
			s.execute();
			s= c.prepareStatement("alter table clients alter column clientID not null");
			s.execute();
			s= c.prepareStatement("alter table clients alter column clientName not null");
			s.execute();
			s= c.prepareStatement("alter table clients alter column redirectURI not null");
			s.execute();

			s= c.prepareStatement("alter table authorizations add foreign key(username) references users(username)");
			s.execute();
			s= c.prepareStatement("alter table authorizations add foreign key(clientID) references clients(clientID)");
			s.execute();
			s= c.prepareStatement("alter table authorizations add unique (username,clientID)");
			s.execute();
			s= c.prepareStatement("alter table authorizations add unique (authcode)");
			s.execute();
			s= c.prepareStatement("alter table authorizations add unique (accesstoken)");
			s.execute();
			s= c.prepareStatement("alter table authorizations add unique (refreshcode)");
			s.execute();
			s= c.prepareStatement("alter table authorizations alter column username not null");
			s.execute();
			s= c.prepareStatement("alter table authorizations alter column clientid not null");
			s.execute();
			s= c.prepareStatement("alter table authorizations alter column authcode not null");
			s.execute();
			s= c.prepareStatement("alter table authorizations alter column redirecturi not null");
			s.execute();
			s= c.prepareStatement("alter table authorizations alter column accesstoken not null");
			s.execute();
			s= c.prepareStatement("alter table authorizations alter column refreshcode not null");
			s.execute();
			s= c.prepareStatement("alter table authorizations alter column isused not null");
			s.execute();
			s= c.prepareStatement("alter table authorizations alter column authexpire not null");
			s.execute();
			s= c.prepareStatement("alter table authorizations alter column accessexpire not null");
			s.execute();
			s= c.prepareStatement("alter table authorizations alter column refreshexpire not null");
			s.execute();

			Log.log("creato nuovo database con successo " + databaseName);
			created = true;
			
		} catch (SQLException e) {
			Log.log("eccezzione nella creazione del database " + databaseName);
			e.printStackTrace();
			created=false;
		}
		
		return created;
	}
}