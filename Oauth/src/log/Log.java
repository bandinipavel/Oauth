package log;

/**
 *	Classe per la gestione del logging.
 *
 *	(usato esclusivamente per motivi di debug)
 * 
 */
public class Log {
	private static final boolean active = true;
	/*
	 * Se � stato impostato a compile time il campo 'active' a true,
	 * stampa a console la string passata preceduta dal nome della funzione 
	 * e della classe che hanno invocato questo metodo.
	 */
	public static void log(String s) {
		if(active) {
			StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
			String callingClass = stackTraceElements[2].getClassName();
			String callingMethod = stackTraceElements[2].getMethodName();
			
			System.out.println(callingClass+"."+callingMethod + ":		" + s);
		}
	}
	
}
