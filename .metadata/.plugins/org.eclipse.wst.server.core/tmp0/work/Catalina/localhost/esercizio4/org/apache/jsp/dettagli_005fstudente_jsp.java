package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.LinkedList;
import it.unipr.informatica.esercizio4.modello.Studente;
import java.util.List;

public final class dettagli_005fstudente_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>Sistema di Gestione dell'Ateneo</title>\r\n");
      out.write("<script src=\"funzioni.js\" type=\"text/javascript\"></script>\r\n");
      out.write("<link href=\"stile.css\" rel=\"stylesheet\" />\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
 
	Studente studente = (Studente)session.getAttribute("studente");

	session.removeAttribute("studente");

      out.write("\r\n");
      out.write("<h1>Sistema di Gestione dell'Ateneo</h1>\r\n");
      out.write("<form id=\"form\" action=\"aggiorna_studente\" method=\"post\">\r\n");
      out.write("<p>Matricola: ");
      out.print(studente.getMatricola() );
      out.write("</p>\r\n");
      out.write("<p>Cognome: <input id=\"cognome\" type=\"text\" name=\"cognome\" value=\"");
      out.print(studente.getCognome() );
      out.write("\" /> <span id=\"errore_cognome\" class=\"campo_nascosto\">Lunghezza non ammissibile</span></p>\r\n");
      out.write("<p>Nome: <input id=\"nome\" type=\"text\" name=\"nome\" value=\"");
      out.print(studente.getNome() );
      out.write("\" /> <span id=\"errore_nome\" class=\"campo_nascosto\">Lunghezza non ammissibile</span></p>\r\n");
      out.write("<p><a href=\"javascript:valida()\">Aggiorna studente</a></p>\r\n");
      out.write("<input type=\"hidden\" name=\"matricola\" value=\"");
      out.print(studente.getMatricola() );
      out.write("\" />\r\n");
      out.write("</form>\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else log(t.getMessage(), t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
