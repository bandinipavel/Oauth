package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import database.DatabaseManager;
import oauthStrings.OauthStrings;
import database.Authorization;
import java.util.List;
import java.sql.Timestamp;

public final class authorization_005flist_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write(" \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>Elenco degli Autorizzazioni concesse</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");

	
	DatabaseManager databaseManager;
	if(request.getSession().getAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER) != null){
		databaseManager = (DatabaseManager) request.getSession().getAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER);
	} else {
		databaseManager = new DatabaseManager();
	}

	
      out.write("\r\n");
      out.write("<table border=\"1\">\r\n");
      out.write("\t<tr><td>Username</td><td>ClientID</td><td>AuthCode</td><td>AccessCode</td>\r\n");
      out.write("\t<td>RefreshCode</td><td>RedirectURI</td><td>Issued</td><td>authExpire</td><td>accessExpire</td><td>refreshExpire</td>\r\n");

	List<Authorization> authList = databaseManager.getAllAuthorization(); 
	for(int i=0; i<authList.size(); i++) {

		String username = authList.get(i).username;
		long clientName = authList.get(i).clientID;
		String authCode = authList.get(i).authCode;
		String refreshToken = authList.get(i).refreshToken;
		String accessCode = authList.get(i).accessCode;
		String redirectURI = authList.get(i).redirectURI;
		boolean issued = authList.get(i).issued;
		Timestamp authExpire   = 	authList.get(i).authExpire;
		Timestamp accessExpire =   authList.get(i).accessExpire;
		Timestamp refreshExpire =  authList.get(i).refreshExpire;
		

      out.write("\r\n");
      out.write("<tr>\r\n");
      out.write("<td><p>");
      out.print(username );
      out.write("</p></td> <td><p>");
      out.print(clientName );
      out.write("</p></td> <td><p>");
      out.print(authCode );
      out.write("</p></td> \r\n");
      out.write("<td><p>");
      out.print(accessCode  );
      out.write("</p></td> <td><p>");
      out.print(refreshToken);
      out.write("</p></td> <td><p>");
      out.print(redirectURI );
      out.write("</p></td><td><p>");
      out.print(issued );
      out.write("</p></td>\r\n");
      out.write("<td><p>");
      out.print(authExpire );
      out.write("</p></td><td><p>");
      out.print(accessExpire );
      out.write("</p></td><td><p>");
      out.print(refreshExpire );
      out.write("</p></td>\r\n");
      out.write("</tr>\r\n");
      out.write("\r\n");

	}

      out.write("\r\n");
      out.write("</table>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else log(t.getMessage(), t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
