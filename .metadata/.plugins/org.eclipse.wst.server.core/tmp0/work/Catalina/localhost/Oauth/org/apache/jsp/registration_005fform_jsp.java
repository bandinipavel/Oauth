package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import oauthStrings.OauthStrings;

public final class registration_005fform_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>Client Registration Form</title>\r\n");

//variabili locali per accorciare le stringhe referenti ai name del form..
String clientName = OauthStrings.ClientRegistrationParameter.CLIENT_NAME;
String clientID = OauthStrings.ClientRegistrationParameter.CLIENT_ID;
String redirectURI = OauthStrings.ClientRegistrationParameter.REDIRECT_URI;

      out.write("\r\n");
      out.write("\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("function trim(s) {\r\n");
      out.write("\treturn s.replace(/^\\s+|\\s+$/gm, \"\");\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("function verificaCampo() {\r\n");
      out.write("\t\r\n");
      out.write("\tvar submit = document.getElementById(\"submit\");\r\n");
      out.write("\tvar nodo = document.getElementById(\"redirect_uri\");\r\n");
      out.write("\tvar testo = nodo.value;\r\n");
      out.write("\tvar errore = document.getElementById(\"errore_redirecturi\");\r\n");
      out.write("\tvar ok = true;\r\n");
      out.write("\r\n");
      out.write("\tif(trim(testo) == \"\") {\r\n");
      out.write("\t\terrore.innerHTML = \"RedirectURI non può essere vuoto\";\r\n");
      out.write("\t\tsubmit.disabled = \"disabled\";\r\n");
      out.write("\t\tok = false;\r\n");
      out.write("\t} else {\r\n");
      out.write("\t\terrore.innerHTML = \"\";\r\n");
      out.write("\t}\r\n");
      out.write("\t\r\n");
      out.write("\tnodo = document.getElementById(\"client_name\");\r\n");
      out.write("\ttesto = nodo.value;\r\n");
      out.write("\terrore = document.getElementById(\"errore_clientname\");\r\n");
      out.write("\r\n");
      out.write("\tif(trim(testo) == \"\") {\r\n");
      out.write("\t\terrore.innerHTML = \"ClientName non può essere vuoto\";\r\n");
      out.write("\t\tsubmit.disabled = \"disabled\";\r\n");
      out.write("\t\tok = false;\r\n");
      out.write("\t} else {\r\n");
      out.write("\t\terrore.innerHTML = \"\";\r\n");
      out.write("\t}\r\n");
      out.write("\t\r\n");
      out.write("\tif(ok)\r\n");
      out.write("\t\tsubmit.removeAttribute(\"disabled\");\r\n");
      out.write("}\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("<form action=\"client_registration\" method=\"post\">\r\n");
      out.write("<p>ClientName: <input id=\"");
      out.print(clientName);
      out.write("\" name=\"");
      out.print(clientName);
      out.write("\" type=\"text\" value=\"\" onchange=\"verificaCampo()\" />  <span id=\"errore_clientname\"></span></p>\r\n");
      out.write("<p>RedirectURI: <input id=\"");
      out.print(redirectURI);
      out.write("\" name=\"");
      out.print(redirectURI);
      out.write("\" type=\"text\" value=\"\" onchange=\"verificaCampo()\" /> <span id=\"errore_redirecturi\"></span></p>\r\n");
      out.write("<input id=\"submit\" type=\"submit\" value=\"Register client\" disabled=\"disabled\" />\r\n");
      out.write("</form>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else log(t.getMessage(), t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
