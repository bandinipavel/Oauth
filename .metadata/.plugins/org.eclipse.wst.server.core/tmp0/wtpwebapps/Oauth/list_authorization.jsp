<%@page import="database.DatabaseManager"%>
<%@page import="oauthStrings.OauthStrings"%>
<%@page import="database.Authorization"%> 
<%@page import="java.util.List"%>
<%@page import="java.sql.Timestamp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Elenco degli Autorizzazioni concesse</title>
</head>
<body>

<%
	
	DatabaseManager databaseManager;
	if(request.getSession().getAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER) != null){
		databaseManager = (DatabaseManager) request.getSession().getAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER);
	} else {
		databaseManager = new DatabaseManager();
	}

	%>
<table border="1">
	<tr><td>Username</td><td>ClientID</td><td>AuthCode</td><td>AccessCode</td>
	<td>RefreshCode</td><td>IsUsed</td><td>authExpire</td><td>accessExpire</td><td>refreshExpire</td>
<%
	List<Authorization> authList = databaseManager.getAllAuthorization(); 
	for(int i=0; i<authList.size(); i++) {

		String username = authList.get(i).username;
		long clientName = authList.get(i).clientID;
		String authCode = authList.get(i).authCode;
		String refreshToken = authList.get(i).refreshToken;
		String accessCode = authList.get(i).accessCode;
		//String redirectURI = authList.get(i).redirectURI;
		boolean issued = authList.get(i).issued;
		Timestamp authExpire   = 	authList.get(i).authExpire;
		Timestamp accessExpire =   authList.get(i).accessExpire;
		Timestamp refreshExpire =  authList.get(i).refreshExpire;
		
%>
<tr>
<td><p><%=username %></p></td> <td><p><%=clientName %></p></td> <td><p><%=authCode %></p></td> 
<td><p><%=accessCode  %></p></td> <td><p><%=refreshToken%></p></td> <td><p><%=issued %></p></td>
<td><p><%=authExpire %></p></td><td><p><%=accessExpire %></p></td><td><p><%=refreshExpire %></p></td>
<td>
<form action="revoke_authorization" method="post">
<input id="auth_token" name="auth_token" type="hidden" value="<%=authCode%>" /> 
<input id="url" name="url" type="hidden" value="list_authorization.jsp" /> 
<input id="submit" type="submit" value="Revoca" />
</form>
</td>
</tr>

<%
	}
%>
</table>

</body>
</html>