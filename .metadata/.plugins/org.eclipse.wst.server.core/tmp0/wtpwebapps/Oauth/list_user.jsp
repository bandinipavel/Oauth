<%@page import="database.DatabaseManager"%>
<%@page import="oauthStrings.OauthStrings"%>
<%@page import="database.ResourceOwner"%> 
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Elenco degli User registrati</title>
</head>
<body>

<%
	
	DatabaseManager databaseManager;
	if(request.getSession().getAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER) != null){
		databaseManager = (DatabaseManager) request.getSession().getAttribute(OauthStrings.DatabaseParameter.DATABASEMANAGER);
	} else {
		databaseManager = new DatabaseManager();
	}

	String url = (String) request.getAttribute("javax.servlet.forward.request_uri");
	
	%>
<table border="1">
	<tr><td>Username</td><td>PasswordHash</td>
<%
	List<ResourceOwner> userList = databaseManager.getAllResourceOwner();
	for(int i=0; i<userList.size(); i++) {
		String username = userList.get(i).username;
		String password = userList.get(i).passwordHash;

%>
<tr>

<td><p><%=username %></p></td> <td><p><%=password %></p></td> 
<td>
<form action="unregistration_user" method="post">
<input id="username" name="username" type="hidden" value="<%=username%>" /> 
<input id="url" name="url" type="hidden" value="list_user.jsp" /> 
<input id="submit" type="submit" value="Cancella" />
</form>
</td>

</tr>
<%
	}
%>
</table>

</body>
</html>