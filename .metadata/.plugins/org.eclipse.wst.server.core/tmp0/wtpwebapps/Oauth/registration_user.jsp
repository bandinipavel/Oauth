<%@page import="oauthStrings.OauthStrings"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registrazione User </title>
<%
//variabili locali per accorciare le stringhe dei name del form..
String username = OauthStrings.UserRegistrationParameter.USERNAME;
String password = OauthStrings.UserRegistrationParameter.PASSWORD;
%>

<script type="text/javascript">
function trim(s) {
	return s.replace(/^\s+|\s+$/gm, "");
} 

function verificaCampo() {
	
	var submit = document.getElementById("submit");
	var nodo = document.getElementById("password");
	var testo = nodo.value;
	var errore = document.getElementById("errore_password");
	var ok = true;

	if(trim(testo) == "") {
		errore.innerHTML = "La password non pu� essere vuota";
		submit.disabled = "disabled";
		ok = false;
	} else {
		errore.innerHTML = "";
	}
	
	nodo = document.getElementById("username");
	testo = nodo.value;
	errore = document.getElementById("errore_username");

	if(trim(testo) == "") {
		errore.innerHTML = "Username non pu� essere vuoto";
		submit.disabled = "disabled";
		ok = false;
	} else {
		errore.innerHTML = "";
	}
	
	if(ok)
		submit.removeAttribute("disabled");
}
</script>
</head> 
<body>

<form action="registration_user" method="post">
<p>UserName: <input id="<%=username%>" name="<%=username%>" type="text" value="" onchange="verificaCampo()" />  <span id="errore_username"></span></p>
<p>Password: <input id="<%=password%>" name="<%=password%>" type="password" value="" onchange="verificaCampo()" /> <span id="errore_password"></span></p>
<input id="submit" type="submit" value="Registra user" disabled="disabled" />
</form>

</body>
</html>