function valida() {
	cognome = document.getElementById("cognome");
	
	testo = cognome.value;
	
	if(testo.length == 0 || testo.length > 50) {
		cognome.className = "campo_con_errore";
		
		errore = document.getElementById("errore_cognome");
		
		errore.className = "";
		
		return;
	} else {
		cognome.className = "";

		errore = document.getElementById("errore_cognome");
		
		errore.className = "campo_nascosto";
	}

	nome = document.getElementById("nome");
	
	testo = nome.value;
	
	if(testo.length == 0 || testo.length > 50) {
		nome.className = "campo_con_errore";

		errore = document.getElementById("errore_nome");
		
		errore.className = "";

		return;
	} else {
		nome.className = "";

		errore = document.getElementById("errore_nome");
		
		errore.className = "campo_nascosto";
	}
	
	form = document.getElementById("form");
	
	form.submit();
}
