<%@page import="java.util.LinkedList"%>
<%@page import="it.unipr.informatica.esercizio4.modello.Studente"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sistema di Gestione dell'Ateneo</title>
<script src="funzioni.js" type="text/javascript"></script>
<link href="stile.css" rel="stylesheet" />
</head>
<body>
<% 
	Studente studente = (Studente)session.getAttribute("studente");

	session.removeAttribute("studente");
%>
<h1>Sistema di Gestione dell'Ateneo</h1>
<form id="form" action="aggiorna_studente" method="post">
<p>Matricola: <%=studente.getMatricola() %></p>
<p>Cognome: <input id="cognome" type="text" name="cognome" value="<%=studente.getCognome() %>" /> <span id="errore_cognome" class="campo_nascosto">Lunghezza non ammissibile</span></p>
<p>Nome: <input id="nome" type="text" name="nome" value="<%=studente.getNome() %>" /> <span id="errore_nome" class="campo_nascosto">Lunghezza non ammissibile</span></p>
<p><a href="javascript:valida()">Aggiorna studente</a></p>
<input type="hidden" name="matricola" value="<%=studente.getMatricola() %>" />
</form>
</body>
</html>
