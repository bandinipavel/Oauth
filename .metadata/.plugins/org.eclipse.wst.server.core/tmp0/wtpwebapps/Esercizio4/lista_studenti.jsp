<%@page import="java.util.LinkedList"%>
<%@page import="it.unipr.informatica.esercizio4.modello.Studente"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sistema di Gestione dell'Ateneo</title>
<link href="stile.css" rel="stylesheet" />
</head>
<body>
<% 
	List<Studente> studenti = (List<Studente>)session.getAttribute("lista_studenti");

	session.removeAttribute("lista_studenti");

	if(studenti == null)
		studenti = new LinkedList();
%>
<h1>Sistema di Gestione dell'Ateneo</h1>
<table>
<thead>
<tr><td>Matricola</td><td>Cognome</td><td>Nome</td></tr>
</thead>
<tbody>
<% 
	for(Studente studente : studenti) {
%>
<tr>
<td><a href="dettagli_studente?matricola=<%=studente.getMatricola() %>"><%=studente.getMatricola() %></a></td>
<td><a href="dettagli_studente?matricola=<%=studente.getMatricola() %>"><%=studente.getCognome() %></a></td>
<td><a href="dettagli_studente?matricola=<%=studente.getMatricola() %>"><%=studente.getNome() %></a></td>
</tr>
<%
	}
%>
</tbody>
</table>
</body>
</html>
